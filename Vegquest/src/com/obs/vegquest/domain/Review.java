package com.obs.vegquest.domain;

public class Review {

	/**
	 * Name: Review
	 * Use : This class is used for transfer the  Review Information
	 * @author John
	 *
	 */
	
	String userId;
	String username;
	String resturantId;
	String message;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getResturantId() {
		return resturantId;
	}
	public void setResturantId(String resturantId) {
		this.resturantId = resturantId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}



}

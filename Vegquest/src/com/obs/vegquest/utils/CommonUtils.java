package com.obs.vegquest.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.obs.vegquest.activity.Home;
import com.obs.vegquest.domain.Country;
import com.obs.vegquest.domain.Info;
import com.obs.vegquest.domain.Restaurant;
import com.obs.vegquest.domain.Review;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.AndroidHttpClient;
import android.text.Html;
import android.util.JsonReader;
import android.util.Log;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Name: CommonUtils
 * Use : This is used for  common purpose.
 * @author John
 *
 */
public class CommonUtils  implements CommonConstants{
	ConnectivityManager cm;
	NetworkInfo  netInfo;
	// the timeout until a connection is established
	private static final int CONNECTION_TIMEOUT = 5000; /* 5 seconds */

	// the timeout for waiting for data
	private static final int SOCKET_TIMEOUT = 5000; /* 5 seconds */

	// ----------- this is the one I am talking about:
	// the timeout until a ManagedClientConnection is got 
	// from ClientConnectionRequest
	private static final long MCC_TIMEOUT = 5000; /* 5 seconds */

	final static String SYSTEM_NEWLINE  = "\n";
	final static float COMPLEXITY = 5.12f;  //Reducing this will increase efficiency but will decrease effectiveness
	final static Paint p = new Paint();


	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}


	//Load images from server
	private static Drawable ImageOperations(String url, String saveFilename) {
		try {
			URL imageUrl = new URL(url);
			InputStream is = (InputStream) imageUrl.getContent();
			Drawable d = Drawable.createFromStream(is, "src");
			return d;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}




	//get values from json and update to the user table
	public static ArrayList<Restaurant> loadResturant(JSONArray userJsonArray){
		ArrayList<Restaurant> hotelList =  new ArrayList<Restaurant>();

		try {
			//iterate json array

			if(userJsonArray!=null){

				for (int i = 0; i < userJsonArray.length(); i++) {
					JSONObject objects = userJsonArray.getJSONObject(i);
					Log.i("load Restaurant"+i, objects.toString());
					String id 		= (!objects.isNull("id"))?(String)objects.get("id"):"";
					String name = (!objects.isNull("Restaurant Name"))?(String)objects.get("Restaurant Name"):"";
					String state 	= (!objects.isNull("State"))?(String)objects.get("State"):"";
					String city 	= (!objects.isNull("City"))?(String)objects.get("City"):"";
					String address 	= (!objects.isNull("Address"))?(String)objects.get("Address"):"";
					String zipcode 	= (!objects.isNull("Zipcode"))?(String)objects.get("Zipcode"):"";
					String phone = (!objects.isNull("Phone"))?(String)objects.get("Phone"):"";
					String type = (!objects.isNull("Type"))?(String)objects.get("Type"):"";
					String desc = (!objects.isNull("Description"))?(String)objects.get("Description"):"";
					String rating  = (!objects.isNull("Rating"))?(String)objects.get("Rating"):"0";
					String favourite  = (!objects.isNull("Favourite"))?(String)objects.get("Favourite"):"0";
					String webSite  = (!objects.isNull("Website"))?(String)objects.get("Website"):"";
					String hours  = (!objects.isNull("Hours"))?(String)objects.get("Hours"):"";
					Restaurant hotel = new Restaurant();
					hotel.setId(Integer.parseInt(id));
					hotel.setName(name);
					hotel.setState(state);
					hotel.setCity(city);
					hotel.setAddress(address);
					hotel.setZipcode(zipcode);
					hotel.setPhoneno(phone);
					hotel.setType(type);
					hotel.setDistance(desc);
					hotel.setFavourite(Integer.parseInt(favourite));
					hotel.setWebSite(webSite);
					hotel.setHours(hours);
					hotel.setRating(Float.parseFloat(rating));
					hotelList.add(hotel);

				}

			} 

		} catch (Exception e) {
			Log.e("Error on loadResturant", "Error "+e);
			e.printStackTrace();
			return  null;
		}
		return hotelList;	 
	}

	public static ArrayList<Review> loadReview(JSONArray userJsonArray){
		ArrayList<Review> reviewList = new ArrayList<Review>();
		try {
			//iterate json array

			if(userJsonArray!=null){
				for (int i = 0; i < userJsonArray.length(); i++) {
					JSONObject objects = userJsonArray.getJSONObject(i);
					Log.i("loadReview"+i, objects.toString());
					String userid 		= (!objects.isNull("UserId"))?(String)objects.get("UserId"):"";
					String resturantId = (!objects.isNull("RestaurantId"))?(String)objects.get("RestaurantId"):"";
					String msg 	= (!objects.isNull("Message"))?(String)objects.get("Message"):"";
					Review review = new Review();
					review.setUserId(userid);
					review.setResturantId(resturantId);
					review.setMessage(msg);
					reviewList.add(review);

				} 

			}
		} catch (Exception e) {
			Log.e("Error on LoadReview", "Error "+e);
			e.printStackTrace();
			return  null;
		}

		return reviewList;	  
	}


	//read json value from weblink
	public static JSONArray readJsonFromUrl1(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			Log.i("webserviceCall", jsonText.toString());
			JSONArray the_json_array = new JSONArray(jsonText);

			return the_json_array;
		} finally {
			is.close();
		}
	}


	//check network available
	public static boolean isNetworkAvailable(Context ctx) {
		ConnectivityManager cm = (ConnectivityManager)
				ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		// if no network is available networkInfo will be null otherwise check we are connected
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	//Read Json output from the browser
	public static  JSONArray readJsonArray(String name) {
		JSONArray jArray = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();  
			httpClient.getParams().setIntParameter("http.connection.timeout", 10000);
			HttpPost httppost = new HttpPost(name);
			//setTimeouts(httppost.getParams());
			HttpResponse response = httpClient.execute(httppost);
			HttpEntity entity = response.getEntity(); 
			String result = EntityUtils.toString(entity, HTTP.UTF_8);
			jArray = new JSONArray(result); 
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection "+ e.getMessage());
			// e.printStackTrace();
			//return jArray;
		}finally{

		}
		return jArray;	

	}

	//Read the readJsonObject output
	public static  JSONObject readJsonObject(String string) {
		JSONObject jsonObject = null;
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(string);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			//this line here is what saves a lot of work
			String result = EntityUtils.toString(entity, HTTP.UTF_8);
			jsonObject =new JSONObject(result);

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{

		}
		return jsonObject;
	}

	//Validation for Email
	public final static boolean isValidEmail(CharSequence target) {

		return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

	}
	// check the validation for TextField
	public static boolean checkEmpty(EditText etText)
	{
		if(etText.getText().toString().trim().length() > 0)
			return true;
		else
			return false; 
	}

	// SErver Timeout Settings 

	private static void setTimeouts(HttpParams params) {
		params.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 
				CONNECTION_TIMEOUT);
		params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, SOCKET_TIMEOUT);
		params.setLongParameter(ConnManagerPNames.TIMEOUT, MCC_TIMEOUT);
	}


	//Show the Alert  message
	public static void showMessage(String title,String msg,Context context) 
	{
		AlertDialog.Builder altDialog= new AlertDialog.Builder(context);
		altDialog.setMessage(Html.fromHtml(msg));
		altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				 
				 dialog.cancel();
			}
		});
		 
	}

	

	//Show the Alert  message
	public static void showMessageExit(String title,String msg,Context context,final Activity act) 
	{
		AlertDialog.Builder altDialog= new AlertDialog.Builder(context);
		altDialog.setMessage(Html.fromHtml(msg));
		altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				 
				act.finish();
			}
		});
		altDialog.setNegativeButton(CANCEL_LABEL, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub


				dialog.cancel();
			}
		});
		altDialog.show();
	}


	//justifyText 
	public static void justifyText(final TextView tv, final float origWidth){
		String s = tv.getText().toString();
		p.setTypeface(tv.getTypeface());        
		String [] splits = s.split(SYSTEM_NEWLINE);
		float width = origWidth - 5;
		for(int x = 0; x<splits.length;x++)
			if(p.measureText(splits[x])>width){
				splits[x] = wrap(splits[x], width, p);
				String [] microSplits = splits[x].split(SYSTEM_NEWLINE);
				for(int y = 0; y<microSplits.length-1;y++)
					microSplits[y] = justify(removeLast(microSplits[y], " "), width, p);
				StringBuilder smb_internal = new StringBuilder();
				for(int z = 0; z<microSplits.length;z++)
					smb_internal.append(microSplits[z]+((z+1<microSplits.length) ? SYSTEM_NEWLINE : ""));
				splits[x] = smb_internal.toString();
			}       
		final StringBuilder smb = new StringBuilder();
		for(String cleaned : splits)
			smb.append(cleaned+SYSTEM_NEWLINE);
		tv.setGravity(Gravity.LEFT);
		tv.setText(smb);
	}
	private static String wrap(String s, float width, Paint p){
		String [] str = s.split("\\s"); //regex
		StringBuilder smb = new StringBuilder(); //save memory
		smb.append(SYSTEM_NEWLINE);
		for(int x = 0; x<str.length; x++){
			float length = p.measureText(str[x]);
			String [] pieces = smb.toString().split(SYSTEM_NEWLINE);
			try{
				if(p.measureText(pieces[pieces.length-1])+length>width)         
					smb.append(SYSTEM_NEWLINE);
			}catch(Exception e){}
			smb.append(str[x] + " ");
		}
		return smb.toString().replaceFirst(SYSTEM_NEWLINE, "");
	}

	// removeLast character
	private static String removeLast(String s, String g){
		if(s.contains(g)){
			int index = s.lastIndexOf(g);
			int indexEnd = index + g.length();
			if(index == 0) return s.substring(1);
			else if(index == s.length()-1)  return s.substring(0, index);
			else
				return s.substring(0, index) + s.substring(indexEnd);
		}
		return s;
	}

	//justifyOperation
	private static String justifyOperation(String s, float width, Paint p){
		float holder = (float) (COMPLEXITY*Math.random());
		while(s.contains(Float.toString(holder)))
			holder = (float) (COMPLEXITY*Math.random());
		String holder_string = Float.toString(holder);
		float lessThan = width;
		int timeOut = 100;
		int current = 0;
		while(p.measureText(s)<lessThan&&current<timeOut) {
			s = s.replaceFirst(" ([^"+holder_string+"])", " "+holder_string+"$1");
			lessThan = p.measureText(holder_string)+lessThan-p.measureText(" ");
			current++;          
		}
		String cleaned = s.replaceAll(holder_string, " ");
		return cleaned;
	}
	private static String justify(String s, float width, Paint p){
		while(p.measureText(s)<width){
			s = justifyOperation(s,width, p);
		}
		return s;
	}
	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}
}

package com.obs.vegquest.activity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

public class AboutUs extends Activity {

	// UI controls
	Button backButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.aboutus);
		//controls object instantiation
		backButton = (Button)findViewById(R.id.backButton);

		// backButton is used in header layout
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				Bundle bundle = getIntent().getExtras();
				String from =bundle.getString("from");
				Intent myIntent = getIntent();
				if(from!=null) {
					if(from.equals("menupage")) //menu page
						myIntent.setClass(view.getContext(), Home.class);
					else if(from.equals("searchpage")) //searchpage
						myIntent.setClass(view.getContext(), Search.class);
					else if(from.equals("listingpage"))  //listingpage
						myIntent.setClass(view.getContext(), HotelList.class);
					else if(from.equals("detailpage")) //detailpage
						myIntent.setClass(view.getContext(), HotelDetail.class);
					else if(from.equals("noresult")) //noresult
						myIntent.setClass(view.getContext(), NoResult.class);
					else if(from.equals("reviewpage")) //reviewpage
						myIntent.setClass(view.getContext(), WriteReview.class);
					else if(from.equals("forgotpass"))//forgotpass
						myIntent.setClass(view.getContext(), Login.class);

					startActivity(myIntent);
				} 
			}
		});	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_about_us, menu);
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

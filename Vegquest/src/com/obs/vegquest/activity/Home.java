package com.obs.vegquest.activity;


import java.util.Set;
import java.util.regex.Pattern;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class Home extends Activity implements CommonConstants {

	//UI controls declaration
	ImageView home;
	ImageView favorites;
	ImageView review;
	ImageView settingsImg;
	ImageView contactus;
	ImageView suggest;
	ImageView aboutusButton;
	Button login;
	TextView welcomeLabel;
	Context context;
	Activity activity; 
	//global variables
	String userId ;
	String userName ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.home);
		// find the id
		// UI controls instantiation
		login = (Button)findViewById(R.id.login);
		home = (ImageView)findViewById(R.id.home);
		favorites = (ImageView)findViewById(R.id.favorites);
		review = (ImageView)findViewById(R.id.review);
		settingsImg = (ImageView)findViewById(R.id.settings);
		contactus = (ImageView)findViewById(R.id.contactus);
		suggest = (ImageView)findViewById(R.id.suggest);
		aboutusButton = (ImageView)findViewById(R.id.aboutus);
		welcomeLabel  =(TextView)findViewById(R.id.welcomeLabel);
		context = this;
		activity = this;
		// Get the information from Bundle
		Bundle b =getIntent().getExtras();  
		if(b!=null){

		} 

		/**
		 * Get the information from Shared prefernce
		 * It is mainly used for storing Object in session 
		 */
		SharedPreferences settings = getSharedPreferences("userObject", 0);
		if(settings!=null) {
			if(settings.getString("userId", null)!=null || settings.getString("userName", null)!=null && settings.getBoolean("isLoggedin", Boolean.FALSE) ) {
				userName = settings.getString("userName",null);
				userId = settings.getString("userId",null);
				String displayName = "Welcome "+Html.fromHtml("<b>"+userName+"</b>");
				welcomeLabel.setText(displayName);
				welcomeLabel.setVisibility(View.VISIBLE);
				login.setText("Logout") ;
			}
		}	 

		// login button action
		login.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				if(login.getText().toString().equals("Logout")){
					//Storing the location and settiing object in session
					SharedPreferences settings = getSharedPreferences("userObject", 0);
					SharedPreferences.Editor editor = settings.edit();

					editor.putString("userId", null);
					editor.putString("userName",null);
					editor.putBoolean("isLoggedin", false);
					editor.commit();
					SharedPreferences searchSettings = getSharedPreferences("searchObject", 0);
					SharedPreferences.Editor editor1 = searchSettings.edit();
					editor1.putString("criteria", null);
					editor1.putString("location",null);
					editor1.putString("miles",null);
					editor1.putString("category",null);
					editor1.putString("name",null);
					editor1.putBoolean("isLoggedin", false);
					editor1.commit();
					Intent myIntent = new Intent(); //Navigation to  Home page
					myIntent.setClass(getApplicationContext(), Home.class);
					startActivity(myIntent);
				}else{
					Intent myIntent = new Intent();  //Navigation to Home page
					myIntent.setClass(getApplicationContext(), Login.class);
					startActivity(myIntent);
				}
			}
		});

		// start button action
		suggest.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				if(userId!=null){
					Intent myIntent = new Intent();
					myIntent.setClass(getApplicationContext(), SuggestResturant.class);
					startActivity(myIntent);
				}else{
					AlertDialog.Builder altDialog= new AlertDialog.Builder(Home.this);
					altDialog.setMessage(Html.fromHtml(LOGIN_LABEL));
					altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							Intent intent = getIntent();
							intent.setClass(getApplicationContext(),Login.class);
							startActivity(intent);

						}
					});
					altDialog.setNegativeButton(CANCEL_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub


							dialog.cancel();
						}
					});

					altDialog.show();

				}

			}
		});

		// aboutus button action
		aboutusButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "aboutusButton   clicked",Toast.LENGTH_SHORT).show();
				Intent myIntent = new Intent(getBaseContext(), AboutUs.class);
				myIntent.putExtra("from", "menupage");
				startActivity(myIntent);
			}
		});

		// home button action
		home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub

				Intent intent = getIntent();
				intent.setClass(getApplicationContext(), Search.class);
				startActivity(intent);


			}
		});
		// search button action
		settingsImg.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Navigate to Settings
				Intent intent = getIntent();
				intent.setClass(getApplicationContext(), Settings.class);
				startActivity(intent);
			}
		});

		// favourites button action
		favorites.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				if(userId!=null ||  userName!=null){
					Intent intent = getIntent();
					intent.setClass(getApplicationContext(), FavouritesList.class);
					startActivity(intent);
				}else {

					AlertDialog.Builder altDialog= new AlertDialog.Builder(Home.this);
					altDialog.setMessage(Html.fromHtml(LOGIN_LABEL));
					altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							Intent intent = getIntent();
							intent.setClass(getApplicationContext(),Login.class);
							startActivity(intent);

						}
					});
					altDialog.setNegativeButton(CANCEL_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub


							dialog.cancel();
						}
					});

					altDialog.show();
				}

			}
		});

		// review button action
		review.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				Intent intent = getIntent();
				intent.setClass(getApplicationContext(), ReviewTab.class);
				startActivity(intent);
			}
		});	

		// contactus button action
		contactus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				if(userId!=null ||  userName!=null){

					Intent intent = new Intent();
					intent.setClass(getApplicationContext(), ContactAdmin.class);
					startActivity(intent);
				}	else {

					AlertDialog.Builder altDialog= new AlertDialog.Builder(Home.this);
					altDialog.setMessage(Html.fromHtml(LOGIN_LABEL));
					altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							Intent intent = getIntent();
							intent.setClass(getApplicationContext(),Login.class);
							startActivity(intent);

						}
					});
					altDialog.setNegativeButton(CANCEL_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub


							dialog.cancel();
						}
					});

					altDialog.show();
				}

			}
		});	

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.activity_home, menu);
		return true;
	} 

	// avoid the Splash screen
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	    CommonUtils.showMessageExit("",EXIT_LABEL,context,activity);
	} 

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.obs.vegquest.adapter.FavouriteAdapter;
import com.obs.vegquest.adapter.HotelAdapter;
import com.obs.vegquest.adapter.ReviewAdapter;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class FavouritesList extends Activity  implements CommonConstants{

	//UI controls declaration
	ImageView aboutus;
	Button backButton;
	ListView favouriteList;
	ArrayList<HashMap<String, String>> favouriteHotelList = null;
	TextView noResult;
	TextView hotelName;

	//Adapter class declaration
	FavouriteAdapter adapter;

	//global  variables
	int hotelId;
	String hotelname;
	String URL ;
	String arr[];
	String userId;
	String from;

	static ProgressDialog progress = null;
	JSONArray hotelArray = new JSONArray();
	Context context;
	private FavouriteAdapter favoriteAdapter; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.favouriteslist);

		// UI controls instantiation
		URL = getResources().getString(R.string.baseURL);
		backButton =(Button)findViewById(R.id.backButton) ;
		aboutus =(ImageView)findViewById(R.id.aboutus) ;
		favouriteList =(ListView)findViewById(R.id.favouriteList) ;
		hotelName =(TextView)findViewById(R.id.hotelNameLabel);
		noResult = (TextView)findViewById(R.id.noFav);
		favouriteHotelList =  new ArrayList<HashMap<String, String>>();
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		context = this;

		// Get the information from Bundle
		Bundle b = getIntent().getExtras();
		try {

			/**
			 * Get the information from Shared prefernce
			 * It is mainly used for storing Object in session 
			 */
			SharedPreferences settings = getSharedPreferences("userObject", 0);
			if(settings!=null) {
				if(settings.getString("userId", null)!=null || settings.getString("userName", null)!=null && settings.getBoolean("isLoggedin", Boolean.FALSE) ) {
					userId  = settings.getString("userId", null);

				}
			}		


			// Check the Netowk is Available or not
			if(CommonUtils.isNetworkAvailable(getBaseContext())){
				/* if(getIntent().getExtras()!=null){*/

				progress.show(); //Loading Progressbar
				Thread dataload = new Thread(){ //Thread declaration
					@Override
					public void run() {
						//Garbage collection
						System.gc(); 
						hotelArray = CommonUtils.readJsonArray(URL+"listfavhotels?userId="+URLEncoder.encode(""+userId));
						favouriteHotelList =  loadFavorites(hotelArray);
						sendMessage(favouriteHotelList);
						progress.dismiss();
						interrupt();
					}
				};
				dataload.start(); //start the Thread
			}		 

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		// List Item Click event for single a list row
		favouriteList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {


				HashMap<String, String> v = favouriteHotelList.get(position);
				if(v!=null){
					if(v.containsKey("userName")){

						Intent intent = getIntent(); //Pass the Information
						intent.putExtra("username", v.get("userName"));
						intent.putExtra("hotelName", v.get("hotelName"));
						intent.putExtra("from", "favouritesList");
						intent.putExtra("userId", userId);
						intent.putExtra("hotelId",Integer.parseInt(v.get("hotelId")));
						intent.setClass(getApplicationContext(),HotelDetail.class);
						startActivity(intent);

					}
				}


			}
		});	

		//Back button action
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = getIntent();
				intent.putExtra("userId", userId);
				intent.setClass(getApplicationContext(), Home.class);
				startActivity(intent);

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_favourites_list, menu);
		return true;
	}


	//to handle msg which is send from thread. and update to the textview
	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();
				ArrayList statusMsg = b.getParcelableArrayList("restaurant");
				favoriteAdapter = new FavouriteAdapter(context,statusMsg,favouriteList,noResult);  

				if(favouriteList!=null){
					favouriteList.setAdapter(favoriteAdapter);
					if(favouriteHotelList.size()==0){
						noResult.setVisibility(View.VISIBLE);
						favouriteList.setVisibility(View.INVISIBLE);
					} else{
						noResult.setVisibility(View.INVISIBLE);
						favouriteList.setVisibility(View.VISIBLE);
					}
				}else{

				}
			}catch (Exception e) {
				// TODO: handle exception
			}	

		}

	};

	public void sendMessage(ArrayList resList){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putParcelableArrayList("restaurant", resList);
		msg.setData(b);
		handler.sendMessage(msg);
	}

	/**
	 * Method Name:loadFavorites
	 * Desc : This methos  is used for loading favorites  
	 * @param hotelArray
	 * @return
	 */
	private ArrayList loadFavorites(JSONArray hotelArray){
		try{
			if(hotelArray!=null){
				for (int i = 0; i < hotelArray.length(); i++) {

					JSONObject objects = hotelArray.getJSONObject(i);

					String favId 		= (!objects.isNull("FavId"))?(String)objects.get("FavId"):"";
					String userId	= (!objects.isNull("UserId"))?(String)objects.get("UserId"):"";
					String resturantId = (!objects.isNull("RestaurantId"))?(String)objects.get("RestaurantId"):"";
					String restaurantName = (!objects.isNull("Restaurant Name"))?(String)objects.get("Restaurant Name"):""; 
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("userName",userId);
					map.put("hotelName",restaurantName);
					map.put("hotelId",resturantId);
					favouriteHotelList.add(map);	
				} 
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
		return  favouriteHotelList;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

package com.obs.vegquest.domain;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Name: Country
 * Use : This class act as Generic  model  
 * @author John
 *
 */
public class Country  implements Parcelable  {

	// variables declaration
	private String id;
	private String name;


	public Country(String id,String name) {
		this.id=id;
		this.name=name;
	} 

	public Country(String name) {
		this.name=name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	} 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(id);
		dest.writeString(name);

	} 
	public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {

		@Override
		public Restaurant createFromParcel(Parcel source) {

			// TODO Auto-generated method stub
			return   new Restaurant(source);
		}

		@Override
		public Restaurant[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Restaurant[size];
		}
	};
	public Country(Parcel source) {
		id = source.readString();
		name = source.readString();
	}

}

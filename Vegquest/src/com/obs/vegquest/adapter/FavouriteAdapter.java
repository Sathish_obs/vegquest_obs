package com.obs.vegquest.adapter;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;

import com.obs.vegquest.activity.HotelDetail;
import com.obs.vegquest.activity.Login;
import com.obs.vegquest.activity.R;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FavouriteAdapter extends BaseAdapter  implements CommonConstants{
	private Context context;
	private ArrayList<HashMap<String, String>> data;
	private static LayoutInflater inflater=null;
	String userId;
	static ProgressDialog progress = null;
	int favHotel = 0 ;	
	String hotelId;
	ListView listView;
	TextView noResult;  
	public FavouriteAdapter(Context context, ArrayList<HashMap<String, String>> d,ListView listView,TextView noResult ) {
		this.context = context;
		data=d;
		inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		progress =  new ProgressDialog(context);
		progress.setMessage(context.getResources().getString(R.string.loading));
		SharedPreferences settings = context.getSharedPreferences("userObject", 0);
		if(settings!=null) {
			if(settings.getString("userId", null)!=null || settings.getString("userName", null)!=null && settings.getBoolean("isLoggedin", Boolean.FALSE) ) {
				userId  = settings.getString("userId", null);

			}
		}	

		this.listView=listView;
		this.noResult =noResult;
	}




	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View vi=convertView;
		if(convertView==null)
			vi = inflater.inflate(R.layout.recentlistreview, null);

		TextView title = (TextView)vi.findViewById(R.id.username); // title
		TextView artist = (TextView)vi.findViewById(R.id.review); // artist name
		TextView city = (TextView)vi.findViewById(R.id.city);
		TextView hotelName = (TextView)vi.findViewById(R.id.hotelName);
		ImageView favorite =(ImageView)vi.findViewById(R.id.favourites);
		HashMap<String, String> song = new HashMap<String, String>();
		song = data.get(position);
		hotelId = song.get("hotelId");
		// Setting all values in listview
		title.setText(song.get("hotelName"));
		//artist.setText(song.get("city"));
		artist.setVisibility(View.GONE);
		city.setVisibility(View.GONE);
		hotelName.setVisibility(View.GONE);
		final int pos =position;
		favorite.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				try{


					if(CommonUtils.isNetworkAvailable(context.getApplicationContext())){
						if(userId!=null && !userId.equals("0")) {
							progress.show();
							Thread dataload = new Thread(){
								@Override
								public void run() {
									//Garbage collection
									System.gc();
									String registerUrl = context.getResources().getString(R.string.baseURL)+"favouritehotel?userId="+URLEncoder.encode(userId)+"&hotelId="+URLEncoder.encode(""+hotelId)+"&fav="+favHotel;
									JSONArray jsonObject1 = CommonUtils.readJsonArray(registerUrl);
									favoritePressed(jsonObject1,pos);
									progress.dismiss();
									interrupt();
								}
							};
							dataload.start();

						} else{

							AlertDialog.Builder altDialog= new AlertDialog.Builder(context);
							altDialog.setMessage(""+Html.fromHtml(LOGIN_LABEL));
							altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub

									Intent intent = new Intent();
									intent.setClass(context,Login.class);
									context.startActivity(intent);

								}
							});


							altDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub


									dialog.cancel();
								}
							});

							altDialog.show();
						}
					}	 else {
						Toast.makeText(context.getApplicationContext(),Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
					}

				}catch(Exception e){
					e.printStackTrace();
				}
			}  	
		});   

		return vi;

	}

	public void favoritePressed(JSONArray  jsonObject1,int position){
		try {
			if(jsonObject1!=null){
				if(jsonObject1.getJSONObject(0).getString("status").equals("true")){

					sendMessage(UNFAVOURITE_LABEL,position);

				}
			} 
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
	}



	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();
				Toast.makeText(context.getApplicationContext(), b.getString("message"), Toast.LENGTH_LONG).show();

				((BaseAdapter)listView.getAdapter()).notifyDataSetChanged();

				data.remove(b.getInt("position"));
				if(data.size()==0) {
					noResult.setVisibility(View.VISIBLE);
				}else{
					noResult.setVisibility(View.GONE);
				}

			}catch (Exception e) {
				// TODO: handle exception
			}	

		}

	};

	public void sendMessage(String message,int position){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		b.putInt("position", position);
		msg.setData(b);
		handler.sendMessage(msg);
	} 

}

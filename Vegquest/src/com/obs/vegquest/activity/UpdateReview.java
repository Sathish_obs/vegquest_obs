package com.obs.vegquest.activity;

import java.net.URLEncoder;

import org.json.JSONArray;

import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateReview extends Activity implements CommonConstants {


	Button backButton;

	ImageView aboutus;
	TextView username;
	TextView hotelname;
	EditText review;
	String logginedUserId;
	String userName;
	String hotelName;
	String hotelId;
	String from;
	String criteria;
	RatingBar ratingBar;
	Button submitButton;
	String userId;
	static ProgressDialog progress = null;
	int reviewId;
	String URL ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.writereview);
		username = (TextView)findViewById(R.id.userName);
		review = (EditText)findViewById(R.id.review);
		hotelname = (TextView)findViewById(R.id.hotelName);
		backButton = (Button)findViewById(R.id.backButton);
		ratingBar =(RatingBar)findViewById(R.id.rating);
		ratingBar.setVisibility(View.GONE);
		submitButton =(Button)findViewById(R.id.submit);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		URL = getResources().getString(R.string.baseURL);
		submitButton.setText("Update");
		username.setVisibility(View.VISIBLE);

		SharedPreferences settings = getSharedPreferences("userObject", 0);
		if(settings!=null) {
			if(settings.getString("userId", null)!=null || settings.getBoolean("isLoggedin", Boolean.FALSE) ) {

				logginedUserId = settings.getString("userId",null);
			}
		}		

		Bundle b = getIntent().getExtras();
		if(b!=null){

			userName = b.getString("username");
			userId = b.getString("userId");
			hotelName = b.getString("hotelName");
			hotelId =b.getString("hotelId");
			from = b.getString("from");
			username.setText(""+userName);
			review.setText(""+b.getString("review"));
			hotelname.setText(""+hotelName);
			criteria =  b.getString("criteria");
			reviewId =b.getInt("reviewId");

		}else{
			username.setText("");
			review.setText("");
		}

		if((logginedUserId!=null && userId!=null) && logginedUserId.equals(userId) ){

			review.setEnabled(true);
			submitButton.setEnabled(true);
		}else{
			review.setEnabled(false);
			submitButton.setVisibility(View.INVISIBLE);
		}
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = getIntent();
				if(from!=null &&  from.equals("reviewlist")){
					intent.setClass(getApplicationContext(), ReviewList.class);
				}else{
					intent.setClass(getApplicationContext(), ViewReview.class);
				}

				intent.putExtra("userId", userId);
				intent.putExtra("hotelName", hotelName);
				intent.putExtra("criteria", criteria);
				intent.putExtra("hotelId", Integer.parseInt(hotelId));
				startActivity(intent);*/
				finish();

			}
		});

		submitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				if(CommonUtils.checkEmpty(review)){

					if(CommonUtils.isNetworkAvailable(getBaseContext())){
						progress.show();
						Thread dataload = new Thread(){
							@Override
							public void run() {
								//Garbage collection
								System.gc();	 

								JSONArray jsonObject = CommonUtils.readJsonArray(URL+"updatereview?userId="+URLEncoder.encode(userId)+"&hotelId="+URLEncoder.encode(""+hotelId)+"&review="+URLEncoder.encode(""+review.getText().toString().trim())+"&reviewId="+URLEncoder.encode(""+reviewId));


								try {
									if(jsonObject.get(0)!=null){
										if(jsonObject.getJSONObject(0).getString("status").equals("true")){
											sendMessage(""+Html.fromHtml(REVIEWUPDATE_SUCCESS));
										} else{
											sendMessage("Failure");
										}
									}
								}catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace() ;
								}	
								progress.dismiss();
								interrupt();
							}
						};
						dataload.start();	      

					}	else{

						Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
					}


				}else{
					AlertDialog.Builder altDialog= new AlertDialog.Builder(UpdateReview.this);
					altDialog.setMessage(""+Html.fromHtml(VALIDATION_LABEL));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show();
				}


			}

		});	


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_review_detail, menu);
		return true;
	}





	Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();
				Toast.makeText(getApplicationContext(),b.getString("msg"), Toast.LENGTH_SHORT).show();
				Intent intent = new Intent();
				intent.putExtra("hotelId",Integer.parseInt(hotelId));
				intent.putExtra("hotelName",hotelName);
				intent.putExtra("from","updatereview");
				intent.putExtra("criteria",criteria);
				intent.setClass(getApplicationContext(),HotelDetail.class);
				startActivity(intent);

			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}	
		}
	};




	public void sendMessage(String message){
		Message msg = new Message();
		Bundle b = new Bundle();

		b.putString("msg", message);
		msg.setData(b);
		handler.sendMessage(msg);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}



}

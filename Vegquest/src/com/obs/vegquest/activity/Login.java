package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.json.JSONArray;

import com.obs.vegquest.adapter.HotelAdapter;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;
import com.obs.vegquest.utils.SharedPreference;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.text.util.Linkify;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity implements CommonConstants {

	EditText userName;
	EditText password;
	Button signin;
	Button register;
	TextView forgotpass;
	Button backButton;

	static ProgressDialog progress = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login);

		userName =(EditText)findViewById(R.id.userName);
		password =(EditText)findViewById(R.id.password);
		signin =(Button)findViewById(R.id.signin);
		register =(Button)findViewById(R.id.register);
		forgotpass =(TextView)findViewById(R.id.forgot);
		backButton = (Button)findViewById(R.id.backButton);
		Pattern pattern = Pattern.compile(getResources().getString(R.string.forgotpass));
		Linkify.addLinks(forgotpass, pattern, "forgotpass://");
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));

		signin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loginPressed();
			}	 
		});

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(getApplicationContext(), Home.class);
				startActivity(intent);
			}	 
		});
		password.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View view, int keyCode, KeyEvent event) {
				if (event != null && keyCode == KeyEvent.KEYCODE_ENTER) {
					loginPressed();

					return true;
				}else{
					return false;
				}	
			}
		});  
		register.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = getIntent();
				i.setClass(getApplicationContext(), Register.class);
				startActivity(i);
			}
		});		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}


	public void loginPressed(){
		if(!CommonUtils.checkEmpty(userName) || !CommonUtils.checkEmpty(password)){
			AlertDialog.Builder altDialog= new AlertDialog.Builder(Login.this);
			altDialog.setMessage("Please fill all the details");
			altDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

					dialog.cancel();
				}
			});
			altDialog.show();

		}else if(CommonUtils.isNetworkAvailable(getBaseContext())){
			progress.show();
			Thread dataload = new Thread(){
				@Override
				public void run() {
					//Garbage collection
					System.gc();
					//redirection
					String registerUrl = getResources().getString(R.string.baseURL)+"login?username="+URLEncoder.encode(userName.getText().toString().trim())+"&password="+URLEncoder.encode(password.getText().toString().trim());

					JSONArray jsonObject1 = CommonUtils.readJsonArray(registerUrl);
					try {
						if(jsonObject1!=null){
							if(jsonObject1.getJSONObject(0).getString("status").equals("true")){
								Intent i = getIntent();
								i.putExtra("userName", jsonObject1.getJSONObject(0).getString("userName").toString());
								i.putExtra("userId", jsonObject1.getJSONObject(0).getString("userId").toString());

								SharedPreferences settings = getSharedPreferences("userObject", 0);
								SharedPreferences.Editor editor = settings.edit();

								editor.putString("userId", jsonObject1.getJSONObject(0).getString("userId").toString());
								editor.putString("userName", jsonObject1.getJSONObject(0).getString("userName").toString());
								editor.putBoolean("isLoggedin", true);
								editor.commit();

								i.setClass(getApplicationContext(), Home.class);
								startActivity(i);
							}else{

								sendMessage("Invalid Credentials");

							}
						}else{
							AlertDialog.Builder altDialog= new AlertDialog.Builder(Login.this);
							altDialog.setMessage("Server is not responding");
							altDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub

									dialog.cancel();
								}
							});
							altDialog.show();
							sendMessage("Server is not responding");
						}
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace() ;
					}  

					progress.dismiss();
					interrupt();
				}
			};
			dataload.start();



		}else {

			AlertDialog.Builder altDialog= new AlertDialog.Builder(Login.this);
			altDialog.setMessage(""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL));
			altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

					dialog.cancel();
				}
			});
			altDialog.show();

		}

	}

	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();

				AlertDialog.Builder altDialog= new AlertDialog.Builder(Login.this);
				altDialog.setMessage(b.getString("message"));
				altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.cancel();
					}
				});
				altDialog.show();

			}catch (Exception e) {
				// TODO: handle exception
			}	

		}

	};

	public void sendMessage(String message){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		msg.setData(b);
		handler.sendMessage(msg);
	} 

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

package com.obs.vegquest.activity;

import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ContactAdmin extends Activity implements CommonConstants {


	Button backButton;
	Button submit;
	EditText subject;
	EditText msg;
	String URL = null;
	String userId ;
	static ProgressDialog progress = null;
	static Context context;
	CommonUtils commonUtils;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.contactadmin);
		backButton = (Button)findViewById(R.id.backButton);
		submit = (Button)findViewById(R.id.submit);
		context = this;
		subject = (EditText)findViewById(R.id.subject);
		msg = (EditText)findViewById(R.id.message);
		commonUtils = new CommonUtils();

		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));

		SharedPreferences settings = getSharedPreferences("userObject", 0);
		if(settings!=null) {
			if(settings.getString("userId", null)!=null ) {

				userId = settings.getString("userId",null);
			}
		}	
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(getBaseContext(), Home.class);

				startActivity(myIntent);

			}
		});	

		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				if(!CommonUtils.checkEmpty(subject) || !CommonUtils.checkEmpty(msg)){

					commonUtils.showMessage("", VALIDATIONALL_LABEL, context);
					 
				} else{
					try {
						if(CommonUtils.isNetworkAvailable(getBaseContext())){
							//URL = getResources().getString(R.string.baseURL)+"contactform?subject="+URLEncoder.encode(subject.getText().toString().trim())+"&message="+URLEncoder.encode(msg.getText().toString().trim());
							progress.show();
							Thread dataload = new Thread(){
								@Override
								public void run() {
									//Garbage collection
									System.gc();
									URL = getResources().getString(R.string.baseURL)+"contactform?subject="+URLEncoder.encode(subject.getText().toString().trim())+"&message="+URLEncoder.encode(msg.getText().toString().trim())+"&userId="+URLEncoder.encode(userId);
									JSONArray jsonObject =CommonUtils.readJsonArray(URL);
									try {
										if(jsonObject.get(0)!=null){
											if(jsonObject.getJSONObject(0).getString("status").equals("true")){
												sendMessage(CONTACTUS_SUCCESS);

											}			

										}
									}catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace() ;
									}  	
									progress.dismiss();
									interrupt();
								}
							};
							dataload.start();	
						}else{
							Toast.makeText(getApplicationContext(), ""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL),Toast.LENGTH_LONG).show();
						}
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();

					}
				}
			} 
		});		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_contact_admin, menu);
		return true;
	}

	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();

				AlertDialog.Builder altDialog= new AlertDialog.Builder(ContactAdmin.this);
				altDialog.setMessage(Html.fromHtml(b.getString("message")));
				altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.cancel();
						Intent intent = new Intent();
						intent.setClass(getApplicationContext(),Home.class);
						startActivity(intent);
					}
				});
				altDialog.show();

			}catch (Exception e) {
				// TODO: handle exception
			}	

		}

	};

	public void sendMessage(String message){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		msg.setData(b);
		handler.sendMessage(msg);
	} 

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

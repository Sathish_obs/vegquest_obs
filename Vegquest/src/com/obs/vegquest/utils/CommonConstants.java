package com.obs.vegquest.utils;


/**
 * Name:  CommonConstants
 * Use : Label for Overall application
 * @author user
 *
 */
public interface CommonConstants {

	public final static String REVIEW_LABEL = "<font color='#65794b'>Do you want to Login to write a review?</font>";
	public final static String FAVOURITE_LABEL = "<font color='#65794b'>Added to your favorites.</font>";
	public final static String UNFAVOURITE_LABEL = "<font color='#65794b'>Removed from your favorites.</font>";
	public final static String RATING_LABEL = "You made this restaurant as your Unfavorited.";
	public final static String LOGIN_LABEL = "<b><font color='#65794b'>Do you want to Login?</font></b>"; 
	public final static String YES_LABEL = "Yes";
	public final static String CANCEL_LABEL = "Cancel";
	public final static String OK_LABEL = "Ok";
	public final static String VALIDATIONALL_LABEL = "<font color='#65794b'>Please fill in all details.</font>";
	public final static String VALIDATION_LABEL = "<font color='#65794b'>Please fill the detail.</font>";
	public final static String REGISTER_VALLABEL = "<font color='#65794b'>Please fill in all details.</font>";
	public final static String FORGOT_VALLABEL = "<font color='#65794b'>Please enter your Email address.</font>";
	public final static String NETWORK_UNAVAILABLE_LABEL = "<font color='#65794b'>Cellular Data is Turned Off.  Turn on cellular data or use Wi-Fi to access data.?</font>";
	public final static String EXIT_LABEL = "<font color='#65794b'>Do you want to Exit?</font>";
	public final static String CATEGORY[] = {"VGN","VGT","VGT, VGN-F","VGT, VGN-F, RAW"};
	public final static String DISTANCE[] = {"1","5","20","50","60"};

	public final static String USERNAME_EXISTS = "<font color='#65794b'>Username already exists.</font>";
	public final static String PSSWORD_NOTMATCH = "<font color='#65794b'>Password is not match.</font>";
	public final static String PSSWORD_VAL = 	"<font color='#65794b'>Password should be morethan 4 charcters.</font>";
	public final static String EMAIL_VAL = 	"<font color='#65794b'>It is not a Valid email id.</font>";
	public final static String FILL_ALL = 	"<font color='#65794b'>Please fill all the fields.</font>";
	public final static String SUGGEST_SUCCESS = 	"<font color='#65794b'>Successfully Posted.</font>";
	public final static String CONTACTUS_SUCCESS = 	"<font color='#65794b'>Message sent. Thank you for your valuable input.</font>";
	public final static String REVIEW_SUCCESS = 	"<font color='#65794b'>Review updated Successfully.</font>";
	public final static String REVIEWUPDATE_SUCCESS = 	"<font color='#65794b'>Thank you for your review</font>";
	public final static String SUGGESTVALIDATION_LABEL = "<font color='#65794b'>Please fill the Restaurant name and Location.</font>";
	public final static String SEARCHVALIDATION_LABEL = "<font color='#65794b'>Please fill the City/State or Zip code.</font>";
	public final static String DESC_LABEL = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description.";
	public final static String DESCLIST_LABEL = "VGN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= Vegan<br>VGT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= Vegetarian<br/>VGN-F&nbsp;&nbsp;= Vegan Friendly";
	public final static String FORGOT_SUCCESS = "<font color='#65794b'>Your password has been sent to your mail id.</font>";
	public final static String EMAIL_NOTEXISTS = "<font color='#65794b'>Email id is not exists.</font>";
	public final static String SPLASH_LABEL  =   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As vegans and vegetarians we realized that there is a need for an app that helps us find the best food options.  In the spirit of openness we are sharing this app for free.  We actively solicit your assistance in validating and identifying vegan and vegetarian eateries all around the world.  This will help make this product a collective success.  <br><br> &nbsp;&nbsp;&nbsp;-Bon Appetit!";
	public final static String PASSWORD_CHANGESUCCESS = "<font color='#65794b'>Password changed successfully.</font>";
	
	public final static String GPS_LABEL = "<font color='#65794b'>GPS Settings.</font>";
	public final static String GPS_MESSAGE = "<font color='#65794b'>GPS is not enabled. Do you want to go to settings menu?.</font>";
	public final static String SETTINGS_LABEL = "<font color='#65794b'>Settings</font>";
	
}

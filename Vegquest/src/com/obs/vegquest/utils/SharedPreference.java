package com.obs.vegquest.utils;

import android.app.Activity;
import android.content.SharedPreferences;


/**
 * Name :  SharedPreference
 * Use :Get the information from Shared prefernce .It is mainly used for storing Object in session
 * author: John 
 */

public class SharedPreference {

	//Variables declaration
	public static String userName;
	public static String userId;
	public static boolean isLoggedin;
	public static SharedPreference sharedPreference;

	public SharedPreference(){

	}

	public static SharedPreference  getSharedPreferenceSingleton(){

		if(sharedPreference==null)
			sharedPreference =  new SharedPreference();

		return sharedPreference;
	}


	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String userName) {
		SharedPreference.userName = userName;
	}

	public static String getUserId() {
		return userId;
	}

	public static void setUserId(String userId) {
		SharedPreference.userId = userId;
	}

	public static boolean getIsLoggedin() {
		return isLoggedin;
	}

	public static void isLoggedin(boolean isLoggedin) {
		SharedPreference.isLoggedin = isLoggedin;
	}

	//Save  the value in  SharedPreference(
	public static  void saveSharedPreference(Activity  activity){
		SharedPreferences settings = activity.getSharedPreferences("userObject", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("userId",userId);
		editor.putString("userName",userName);
		editor.putBoolean("isLoggedIn",isLoggedin);
		editor.commit();
	}

	//Get the value from  SharedPreference(
	public  static SharedPreferences getSharedPreference(Activity  activity){

		SharedPreferences settings = activity.getSharedPreferences("userObject", 0);
		if(settings.getString("userId", null)!=null && settings.getString("userName", null)!=null && settings.getBoolean("isLoggedin", isLoggedin) ) {
			userId = settings.getString("userId", null);
			userName = settings.getString("userName", null);
			isLoggedin = settings.getBoolean("isLoggedin", isLoggedin);
		}

		return  settings;

	}

}

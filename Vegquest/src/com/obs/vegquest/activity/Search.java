package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;
import com.obs.vegquest.domain.Country;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Search extends Activity  implements CommonConstants{

	//UI controls declaration
	Button backButton;
	Button search;
	EditText hotelName;
	AutoCompleteTextView country;
	AutoCompleteTextView state;
	AutoCompleteTextView city;
	EditText zipCode;
	Spinner  distance;
	Spinner  category;
	static ProgressDialog progress = null;
	ImageView showCategory;
	TextView  headingLabel;
	ImageView aboutus;
	TextView changePass;
	ImageView findLocation;
	GPSTracker gpsTracker;
	Context context;

	//global variables declaration
	String currentState;
	String currentZipCode;
	boolean countryLoaded;
	boolean stateLoaded;
	boolean cityLoaded;
	String URL;
	ArrayList  countryList;
	String currentCountry;
	String selectedState;
	String selectedStateId ;
	List<String> distanceList = new ArrayList<String>();
	List<String> categoryList = new ArrayList<String>();
	List<String> restaurants = new ArrayList<String>() ;
	ArrayList<Country> stateList;
	ArrayList<Country> cityList ;
	String currentCity;
	String miles;
	String selectedCategory; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings);
		// UI controls instantiation
		zipCode =(EditText)findViewById(R.id.zipCode);
		headingLabel = (TextView)findViewById(R.id.textView1);
		headingLabel.setText("Search");
		backButton =(Button)findViewById(R.id.backButton);
		search =(Button)findViewById(R.id.searchButton);
		hotelName =(EditText)findViewById(R.id.name);
		country  =(AutoCompleteTextView)findViewById(R.id.country);
		state  =(AutoCompleteTextView)findViewById(R.id.state);
		city  =(AutoCompleteTextView)findViewById(R.id.city);
		distance =(Spinner)findViewById(R.id.distance);
		changePass = (TextView)findViewById(R.id.changepass);
		changePass.setVisibility(View.GONE);
		category =(Spinner)findViewById(R.id.category);
		showCategory =(ImageView)findViewById(R.id.showCategory);
		hotelName.setVisibility(View.VISIBLE);
		aboutus =(ImageView)findViewById(R.id.aboutus);
		search.setVisibility(View.VISIBLE);
		findLocation =(ImageView)findViewById(R.id.findLocation);
		gpsTracker = new GPSTracker(this);
		context = this;
		URL =  getResources().getString(R.string.baseURL);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		countryList =  new ArrayList<String>();
		stateList = new ArrayList<Country>();
		cityList = new ArrayList<Country>();
		if(CommonUtils.isNetworkAvailable(this)){ // Check the Network availability
			//progress.setMessage(getResources().getString(R.string.loadingCountry));  
			progress.setMessage(getResources().getString(R.string.loadingStates));
			progress.show();
			Thread dataload = new Thread(){
				@Override
				public void run() {

					try {
						final String loadCountry =URL +"loadCountry";
						final String loadState =URL +"loadState";
						JSONArray stateArray = CommonUtils.readJsonArray(loadState); //Webservice call for loading country
						if(stateArray!=null){
							for (int i = 0; i < stateArray.length(); i++) {

								JSONObject objects = stateArray.getJSONObject(i);
								Log.i("loadCountry"+i, objects.toString());
								String id 		= (!objects.isNull("id"))?(String)objects.get("id"):"";
								String name		= (!objects.isNull("name"))?(String)objects.get("name"):"";
								stateList.add(new Country(id,name));
							} 
							stateLoaded = Boolean.TRUE;
							sendMessage("",stateList);
						}

					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					} 	
					progress.dismiss();
					interrupt();
				}		

			};
			dataload.start();

		}else{
			Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
		}

		// state onclick event
		state.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(final  AdapterView<?> adapter, View arg1,final int pos, long arg3) {
				// TODO Auto-generated method stub
				//Webservice call for loading city
				loadCity(((Country)adapter.getAdapter().getItem(pos)).getId());
			}
		});


		//Find the Gpas location
		findLocation.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(CommonUtils.isNetworkAvailable(context)) {
					if(gpsTracker.canGetLocation()){
						try {  
							Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
							List<Address> addresses = geo.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
							if (addresses.size() > 0) {
								state.setText(addresses.get(0).getAdminArea());
								city.setText(addresses.get(0).getLocality());
							}
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				}else{
					Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
				}
			}
		});


		for(int i=0;i<DISTANCE.length;i++){
			distanceList.add(DISTANCE[i]);
		}
		for(int j=0;j<CATEGORY.length;j++){
			categoryList.add(CATEGORY[j]);
		}
		//distance list initialization
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, distanceList);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		//category list initialization
		ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, categoryList);
		dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		distance.setAdapter(dataAdapter);
		distance.setSelection(2,true);
		category.setSelection(0);
		category.setAdapter(dataAdapter1);

		//State focus listener event 
		state.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					state.setText("");
					city.setText("");
				}
			}
		});



		try {

			SharedPreferences settings = getSharedPreferences("Settings", 0);
			if(settings!=null) {
				if(settings.getString("city", null)!=null) {
					currentCity  = settings.getString("city", null);
					currentCountry  = settings.getString("country", null);
					currentState =settings.getString("state", null);
					selectedStateId =settings.getString("stateId", null);
					currentZipCode =settings.getString("zipCode", null);
					miles =settings.getString("miles", null);
					selectedCategory =settings.getString("category", null);
					state.setText(currentState);
					country.setText(currentCountry);
					city.setText(currentCity);
					zipCode.setText(currentZipCode);
					// check the Miles
					if(miles.equals("1"))
						distance.setSelection(0,true);
					else if(miles.equals("5"))
						distance.setSelection(1,true);
					else if(miles.equals("20"))
						distance.setSelection(2,true);
					else if(miles.equals("50"))
						distance.setSelection(3,true);
					else if(miles.equals("60"))
						distance.setSelection(4,true);

					// check the Category 
					if(selectedCategory.equals("VGN"))
						category.setSelection(0,true);
					else if(selectedCategory.equals("VGT"))
						category.setSelection(1,true);
					else if(selectedCategory.equals("VGT, VGN-F"))
						category.setSelection(2,true);
					else if(selectedCategory.equals("VGT, VGN-F, RAW"))
						category.setSelection(3,true);

					if(state.getText().toString().trim()!=null){
						loadCity(selectedStateId);
					}


				}else{

					if(CommonUtils.isNetworkAvailable(context)) {
						if(gpsTracker.canGetLocation()){ // Get the GPS Location
							try {  

								Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
								List<Address> addresses = geo.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
								if (addresses.size() > 0) {
									state.setText(addresses.get(0).getAdminArea());
									city.setText(addresses.get(0).getLocality());
									country.setText(addresses.get(0).getCountryName());
								}
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}
					}else{
						Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
					}

				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	

		//Back button action
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = getIntent();
				i.setClass(getApplicationContext(), Home.class); //Navigate to Home page
				startActivity(i);

			}
		});
		// aboutus button action
		aboutus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(getBaseContext(), AboutUs.class);
				myIntent.putExtra("from", "searchpage");
				startActivity(myIntent);
			}
		});


		showCategory.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder altDialog= new AlertDialog.Builder(Search.this);
				altDialog.setTitle(""+Html.fromHtml(DESC_LABEL));
				altDialog.setMessage(Html.fromHtml(DESCLIST_LABEL));
				altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				altDialog.show(); 

			}
		});

		//Search button action
		search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(CommonUtils.checkEmpty(state) ||  CommonUtils.checkEmpty(city)|| CommonUtils.checkEmpty(zipCode)) {
					if(CommonUtils.isNetworkAvailable(getApplicationContext())){ // check the Network availability
						searchPressed(); //web service call for search the restaurant
					}else{
						Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();

					}

				}else if( (!CommonUtils.checkEmpty(state) &&  !CommonUtils.checkEmpty(city)) ){ //check the state and city
					// Validaion message display
					AlertDialog.Builder altDialog= new AlertDialog.Builder(Search.this);
					altDialog.setMessage(Html.fromHtml(SEARCHVALIDATION_LABEL));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
					altDialog.show(); 

				}  
			}	

		});

	}

	public void searchPressed(){
		//Store the search Object in session(SharedPreferences)
		SharedPreferences settings = getSharedPreferences("searchObject", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("location", state.getText().toString().trim());
		editor.putString("city", city.getText().toString().trim());
		editor.putString("state", state.getText().toString().trim());
		editor.putString("stateId", selectedStateId);
		editor.putString("zipCode", zipCode.getText().toString().trim());
		editor.putString("criteria", null);
		editor.putString("name",  hotelName.getText().toString().trim());
		editor.putString("miles", distance.getSelectedItem().toString());
		editor.putString("category", category.getSelectedItem().toString());
		editor.commit();
		countryList =null;
		stateList = null;
		Intent intent =getIntent();	
		intent.putExtra("from", "advancesearch");
		intent.putExtra("category",category.getSelectedItem().toString());
		intent.setClass(getApplicationContext(), HotelList.class);
		startActivity(intent);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_search, menu);
		return true;
	}

	//to handle msg which is send from thread. and update to the textview
	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			Bundle b = msg.getData(); //get the value from Bundle
			ArrayList countryList = b.getStringArrayList("countryList");
			boolean flag = b.getBoolean("flag");
			if(countryLoaded && countryList!=null ){
				//List of Country initialization 
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,countryList);
				country.setAdapter(adapter); 
				countryLoaded =Boolean.FALSE;
			} if(stateLoaded && countryList!=null){
				//List of State initialization
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,countryList);
				state.setAdapter(adapter);
				stateLoaded =Boolean.FALSE;
			} if(cityLoaded && countryList!=null){
				//List of City initialization
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,countryList);
				city.setAdapter(adapter); 
				cityLoaded =Boolean.FALSE;
			} 
			String statusMsg = b.getString("message");
			if(statusMsg!=null && !statusMsg.equals("")) //Display the message
				Toast.makeText(getApplicationContext(),statusMsg,Toast.LENGTH_LONG).show();
		}

	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);
		return true;
	}	   

	public void sendMessage(String message,ArrayList countryList){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		b.putStringArrayList("countryList", countryList);
		msg.setData(b);
		handler.sendMessage(msg);
	} 

	public void loadCity(final String id){
		if(CommonUtils.isNetworkAvailable(getApplicationContext())){  //check the Network availability
			progress.setMessage(getResources().getString(R.string.loadingCities));  
			progress.show();
			//Web service call
			Thread dataload = new Thread(){
				@Override
				public void run() {
					try{
						final String loadStates =URL +"loadCity?stateId="+URLEncoder.encode(id);
						JSONArray cityArray = CommonUtils.readJsonArray(loadStates); 
						if(cityArray!=null){
							for (int i = 0; i < cityArray.length(); i++) {

								JSONObject objects = cityArray.getJSONObject(i);
								Log.i("loadUser"+i, objects.toString());

								String id 		= (!objects.isNull("id"))?(String)objects.get("id"):"";
								String name		= (!objects.isNull("name"))?(String)objects.get("name"):""; 
								cityList.add(new Country(id,name));
							}
							cityLoaded =Boolean.TRUE;

							sendMessage("",cityList);
						}	 
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}finally{

					}
					progress.dismiss();
					interrupt();

				}
			};	
			dataload.start();
		}else{
			Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();		 
		}

	}

}

package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;

import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Register extends Activity  implements CommonConstants{

	EditText fname;
	EditText lname;
	EditText name;
	EditText email;
	EditText pass;
	EditText pass1;
	EditText location;
	Button  register;
	RadioGroup genderGroup;
	RadioButton male;
	RadioButton female;
	Button back;
	static ProgressDialog progress = null;
	GPSTracker gpsTracker;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.register);
		fname =(EditText)findViewById(R.id.fname);
		lname =(EditText)findViewById(R.id.lname);
		name =(EditText)findViewById(R.id.name);
		email =(EditText)findViewById(R.id.emailId);
		pass =(EditText)findViewById(R.id.pass);
		pass1 =(EditText)findViewById(R.id.pass1);
		location =(EditText)findViewById(R.id.Location);
		register =(Button)findViewById(R.id.register);
		back = (Button)findViewById(R.id.backButton);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		gpsTracker = new GPSTracker(this);
		context = this;

		if(CommonUtils.isNetworkAvailable(context)) {
			if(gpsTracker.canGetLocation()){
				try {  

					Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
					List<Address> addresses = geo.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);

					if (addresses.size() > 0) {
						//MAP_URL+=""+addresses.get(0).getLocality()+" "+addresses.get(0).getAdminArea()+"&daddr="+destinationAddress+"&z=1";
						location.setText(addresses.get(0).getLocality()+", "+addresses.get(0).getAdminArea());
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}else{
			Toast.makeText(getApplicationContext(),""+Html.fromHtml( NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
		}

		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = getIntent();
				i.setClass(getApplicationContext(), Login.class);
				i.putExtra("user",name.getText().toString());
				startActivity(i);
			}
		});

		register.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				if(!CommonUtils.checkEmpty(fname) ||
						!CommonUtils.checkEmpty(lname) ||
						!CommonUtils.checkEmpty(name) ||
						!CommonUtils.checkEmpty(email) ||
						!CommonUtils.checkEmpty(pass) ||
						!CommonUtils.checkEmpty(pass1) ||
						!CommonUtils.checkEmpty(location)){

					AlertDialog.Builder altDialog= new AlertDialog.Builder(Register.this);
					altDialog.setMessage(""+Html.fromHtml(REGISTER_VALLABEL));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show(); 
				} else if(!CommonUtils.isValidEmail(email.getText())){
					AlertDialog.Builder altDialog= new AlertDialog.Builder(Register.this);
					altDialog.setMessage(""+Html.fromHtml(EMAIL_VAL));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show(); 
				}	 else if(pass.getText().toString().length()<4 || pass1.getText().toString().length()<4){
					AlertDialog.Builder altDialog= new AlertDialog.Builder(Register.this);
					altDialog.setMessage(""+Html.fromHtml(PSSWORD_VAL));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show(); 
				}
				else if(!pass1.getText().toString().equals(pass.getText().toString())){
					AlertDialog.Builder altDialog= new AlertDialog.Builder(Register.this);
					altDialog.setMessage(""+Html.fromHtml(PSSWORD_NOTMATCH));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show(); 
				}else{

					try {

						if(CommonUtils.isNetworkAvailable(getBaseContext())){
							progress.show();
							Thread dataload = new Thread(){
								@Override
								public void run() {
									//Garbage collection
									System.gc(); 
									registerPressed();
									progress.dismiss();
									interrupt();
								}
							};
							dataload.start(); 
						} else{
							AlertDialog.Builder altDialog= new AlertDialog.Builder(Register.this);
							altDialog.setMessage(""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL));
							altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub

									dialog.cancel();
								}
							});
							altDialog.show();
						}
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}finally{

					}

				}

			}	    
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_register, menu);
		return true;
	}


	private void registerPressed() {
		// TODO Auto-generated method stub
		String registerUrl = getResources().getString(R.string.baseURL)+"register?firstname="+URLEncoder.encode(fname.getText().toString().trim())+"&lastname="+URLEncoder.encode(lname.getText().toString().trim())+"&username="+URLEncoder.encode(name.getText().toString().trim())+"&email="+URLEncoder.encode(email.getText().toString().trim())+"&password="+URLEncoder.encode(pass.getText().toString().trim())+"&location="+URLEncoder.encode(location.getText().toString().trim());
		try {
			JSONArray jsonObject1 = CommonUtils.readJsonArray(registerUrl);

			if(jsonObject1.get(0)!=null){
				if(jsonObject1.getJSONObject(0).getString("status").equals("true")){

					Intent i = getIntent();
					i.setClass(getApplicationContext(), Home.class);
					//i.putExtra("userName",name.getText().toString());

					SharedPreferences settings = getSharedPreferences("userObject", 0);
					SharedPreferences.Editor editor = settings.edit();

					editor.putString("userId", jsonObject1.getJSONObject(0).getString("userId").toString());
					editor.putString("userName", jsonObject1.getJSONObject(0).getString("userName").toString());
					editor.putBoolean("isLoggedin", true);
					editor.commit();

					//i.putExtra("userId",name.getText().toString());

					startActivity(i);

				}else{
					sendMessage(jsonObject1.getJSONObject(0).getString("reason"));
				}

			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();

				AlertDialog.Builder altDialog= new AlertDialog.Builder(Register.this);
				altDialog.setMessage(b.getString("message"));
				altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.cancel();
					}
				});
				altDialog.show();

			}catch (Exception e) {
				// TODO: handle exception
			}	

		}

	};

	public void sendMessage(String message){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		msg.setData(b);
		handler.sendMessage(msg);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

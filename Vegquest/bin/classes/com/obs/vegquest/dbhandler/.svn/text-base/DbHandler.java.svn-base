package com.obs.dbhandler;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.obs.pojo.Categories;
import com.obs.pojo.Product;
import com.obs.pojo.ProductImage;
import com.obs.pojo.Serverupdatelog;
import com.obs.pojo.Tags;
import com.obs.pojo.User;
import com.obs.utils.CommonConstants;

public class DbHandler extends SQLiteOpenHelper{

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "empresasctmInventory";

	// Users table name
	private static final String TABLE_USER = "user";

	// Product table name
	private static final String TABLE_PRODUCT = "product";

	// ProductImage table name
	private static final String TABLE_PRODUCTIMAGE = "productimage";

	// category table name
	private static final String TABLE_CATEGORIES = "categories";

	// tags table name
	private static final String TABLE_TAGS = "tags";

	// serverupdatelog table name
	private static final String TABLE_SERVERUPDATELOG = "serverupdatelog";

	private static final long countPerPage = new CommonConstants().countPerPage;

	//common columns
	private static final String KEY_ID       = "id";
	private static final String KEY_UPDATEDON   = "updatedOn";

	// Users Table Columns names
	private static final String KEY_UNAME    = "username";
	private static final String KEY_PWD      = "password";
	private static final String KEY_FULLNAME = "fullname";
	private static final String KEY_EMAIL    = "email";


	// Product Table Columns names
	private static final String KEY_PRODUCTCODE = "productCode";
	private static final String KEY_PRODUCTNAME = "productName";
	private static final String KEY_PRICE       = "price";
	private static final String KEY_DESCRIPTION = "productDescription";
	private static final String KEY_CATEGORY    = "category";
	private static final String KEY_TAG         = "tag";
	private static final String KEY_CELLAR      = "cellar";
	private static final String KEY_AVAILABLE   = "available";

	// Product Table Columns names
	private static final String KEY_PRODUCTID = "productId";
	private static final String KEY_PRODUCTIMAGE = "productImage";

	// Categories Table Columns names
	private static final String KEY_CATEGORYNAME = "categoryName";

	// Tags Table Columns names
	private static final String KEY_TAGNAME = "tagName";

	// serverupdatelog Table Columns names
	private static final String KEY_SERVERUPDATEDON = "serverUpdatedOn";

	public DbHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating All Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		try{
			String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
					+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_UNAME + " TEXT,"+ KEY_PWD + " TEXT,"
					+ KEY_EMAIL + " TEXT,"+ KEY_FULLNAME + " TEXT," + KEY_UPDATEDON + " TEXT" + ")";

			String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_PRODUCT + "("
					+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_PRODUCTCODE + " TEXT,"+ KEY_PRODUCTNAME + " TEXT,"
					+ KEY_PRICE + " TEXT," +KEY_DESCRIPTION + " TEXT," +KEY_CATEGORY + " TEXT,"+KEY_TAG+ " TEXT,"+KEY_CELLAR + " TEXT," +KEY_AVAILABLE + " TEXT," + KEY_UPDATEDON + " TEXT" + ")";

			String CREATE_PRODUCTIMAGE_TABLE = "CREATE TABLE " + TABLE_PRODUCTIMAGE + "("
					+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_PRODUCTID + " TEXT,"+KEY_PRODUCTIMAGE + " BLOB," + KEY_UPDATEDON + " TEXT" + ")";

			String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + TABLE_CATEGORIES + "("
					+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_CATEGORYNAME + " TEXT,"+ KEY_UPDATEDON + " TEXT" + ")";
			
			String CREATE_TAGS_TABLE = "CREATE TABLE " + TABLE_TAGS + "("
					+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_TAGNAME + " TEXT,"+ KEY_UPDATEDON + " TEXT" + ")";

			String CREATE_SERVERUPDATELOG_TABLE = "CREATE TABLE " + TABLE_SERVERUPDATELOG + "("
					+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_SERVERUPDATEDON + " TEXT" + ")";

			db.execSQL(CREATE_USER_TABLE);
			db.execSQL(CREATE_PRODUCT_TABLE);
			db.execSQL(CREATE_PRODUCTIMAGE_TABLE);
			db.execSQL(CREATE_CATEGORIES_TABLE);
			db.execSQL(CREATE_TAGS_TABLE);
			db.execSQL(CREATE_SERVERUPDATELOG_TABLE);

		} catch (Exception e) {
			Log.e("ERROR_ONCREATE", "ERROR_ONCREATE :"+e);
		}
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i("Inventory", "updating latest version");
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTIMAGE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAGS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SERVERUPDATELOG);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	/********* USER ********************/
	// Adding new user
	public void addUser(User user) {

		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put(KEY_ID, user.getId()); // user id
			values.put(KEY_UNAME, user.getUsername()); // user name
			values.put(KEY_PWD, user.getPassword()); // password
			values.put(KEY_EMAIL, user.getEmail()); // email
			values.put(KEY_FULLNAME, user.getFullname()); // full name
			values.put(KEY_UPDATEDON, user.getUpdatedOn()); // UpdatedOn

			// Inserting Row
			db.insert(TABLE_USER, null, values);
		} catch (Exception e) {
			Log.e("ERROR_ADDUSER", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}

	// Getting single user
	public User getUser(int id) {
		User user = null;
		SQLiteDatabase db = this.getReadableDatabase();
		try {
			Cursor cursor = db.query(TABLE_USER, new String[] { KEY_ID,
					KEY_UNAME, KEY_PWD,KEY_EMAIL,KEY_FULLNAME,KEY_UPDATEDON }, KEY_ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			user = new User(Integer.parseInt(cursor.getString(0)),
					cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETUSER", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return user
		return user;
	}

	// Getting All Users
	public List<User> getAllUsers() {
		List<User> userList = new ArrayList<User>();
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_USER;

			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					User user = new User();
					user.setId(Integer.parseInt(cursor.getString(0)));
					user.setUsername(cursor.getString(1));
					user.setPassword(cursor.getString(2));
					user.setEmail(cursor.getString(3));
					user.setFullname(cursor.getString(4));
					user.setUpdatedOn(cursor.getString(5));
					// Adding user to list
					userList.add(user);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETALLUSERS", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return user list
		return userList;
	}

	// Check User login
	public List<User> checkUserLogin(String uname,String pwd) {
		List<User> userList = new ArrayList<User>();
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_USER + " WHERE "+KEY_UNAME+" = '"+uname+"' AND "+KEY_PWD+" = '"+pwd+ "'";

			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					User user = new User();
					user.setId(Integer.parseInt(cursor.getString(0)));
					user.setUsername(cursor.getString(1));
					user.setPassword(cursor.getString(2));
					user.setEmail(cursor.getString(3));
					user.setFullname(cursor.getString(4));
					user.setUpdatedOn(cursor.getString(5));
					// Adding user to list
					userList.add(user);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_CHECKUSERLOGIN", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return user list
		return userList;
	}

	// Updating single user
	public int updateUser(User user) {
		int returnValue = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_UNAME, user.getUsername());
			values.put(KEY_PWD, user.getPassword());
			values.put(KEY_EMAIL, user.getEmail());
			values.put(KEY_FULLNAME, user.getFullname());
			values.put(KEY_UPDATEDON, user.getUpdatedOn()); // UpdatedOn

			// updating row
			returnValue = db.update(TABLE_USER, values, KEY_ID + " = ?",
					new String[] { String.valueOf(user.getId()) });
		} catch (Exception e) {
			Log.e("ERROR_UPDATEUSER", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		return returnValue;
	}

	// Deleting single user
	public void deleteUser(User user) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			db.delete(TABLE_USER, KEY_ID + " = ?",
					new String[] { String.valueOf(user.getId()) });
		} catch (Exception e) {
			Log.e("ERROR_DELETEUSER", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}


	/********* PRODUCT ********************/
	// Adding new product
	public void addProduct(Product product) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, product.getId()); // product id
			values.put(KEY_PRODUCTCODE, product.getProductCode()); // product code
			values.put(KEY_PRODUCTNAME, product.getProductName()); // ProductName
			values.put(KEY_PRICE, product.getPrice()); // Price
			values.put(KEY_DESCRIPTION, product.getDescription()); // Description
			values.put(KEY_CATEGORY, product.getCategory()); // Category
			values.put(KEY_TAG, product.getTag()); // Tag
			values.put(KEY_CELLAR, product.getCellar()); // Cellar
			values.put(KEY_AVAILABLE, product.getAvailable()); // Available
			values.put(KEY_UPDATEDON, product.getUpdatedOn()); // UpdatedOn

			// Inserting Row
			db.insert(TABLE_PRODUCT, null, values);
		} catch (Exception e) {
			Log.e("ERROR_ADDPRODUCT", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}

	// Getting single product
	public Product getProduct(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Product product = null;
		try{
			Cursor cursor = db.query(TABLE_PRODUCT, new String[] { KEY_ID,
					KEY_PRODUCTCODE, KEY_PRODUCTNAME,KEY_PRICE,KEY_DESCRIPTION,KEY_CATEGORY,KEY_TAG,KEY_CELLAR,KEY_AVAILABLE,KEY_UPDATEDON }, KEY_ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			product = new Product(Integer.parseInt(cursor.getString(0)),
					cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),
					cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9));
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETPRODUCTS", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return product
		return product;
	}

	// Getting All Products
	public List<Product> getAllProducts(long pageNo) {
		List<Product> productList = new ArrayList<Product>();
		SQLiteDatabase db = this.getWritableDatabase();
		try{

			long offset = (pageNo*countPerPage)-countPerPage;
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_PRODUCT +" ORDER BY "+KEY_UPDATEDON+" DESC LIMIT "+offset+","+ countPerPage;

			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Product product = new Product();
					product.setId(Integer.parseInt(cursor.getString(0)));
					product.setProductCode(cursor.getString(1));
					product.setProductName(cursor.getString(2));
					product.setPrice(cursor.getString(3));
					product.setDescription(cursor.getString(4));
					product.setCategory(cursor.getString(5));
					product.setTag(cursor.getString(6));
					product.setCellar(cursor.getString(7));
					product.setAvailable(cursor.getString(8));
					product.setUpdatedOn(cursor.getString(9));
					// Adding product to list
					productList.add(product);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETALLPRODUCTS", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return product list
		return productList;
	}

	// Updating single product
	public int updateProduct(Product product) {
		int returnValue = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_PRODUCTCODE, product.getProductCode()); // product code
			values.put(KEY_PRODUCTNAME, product.getProductName()); // ProductName
			values.put(KEY_PRICE, product.getPrice()); // Price
			values.put(KEY_DESCRIPTION, product.getDescription()); // Description
			values.put(KEY_CATEGORY, product.getCategory()); // Category
			values.put(KEY_TAG, product.getTag()); // Tag
			values.put(KEY_CELLAR, product.getCellar()); // Cellar
			values.put(KEY_AVAILABLE, product.getAvailable()); // Available
			values.put(KEY_UPDATEDON, product.getUpdatedOn()); // UpdatedOn

			// updating row
			returnValue = db.update(TABLE_PRODUCT, values, KEY_ID + " = ?",
					new String[] { String.valueOf(product.getId()) });
		} catch (Exception e) {
			Log.e("ERROR_UPDATEPRODUCT", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		return returnValue;
	}

	// search from Products
	public List<Product> searchProducts(long pageNo,String code,String productName,String cellar,String amount,String price,String category,String tag,String amtCond,String priceCond) {
		List<Product> productList = new ArrayList<Product>();
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			long offset = (pageNo*countPerPage)-countPerPage;
			// Select All Query
			String appendQuery = "";
			if(priceCond==null){
				priceCond = "=";
			}
			if(amtCond==null){
				amtCond = "=";
			}
			if(code!=null && !code.trim().equals("")){
				appendQuery = appendQuery+" AND "+KEY_PRODUCTCODE+" LIKE '%"+code.trim()+"%'";
			}
			if(productName!=null && !productName.trim().equals("")){
				appendQuery = appendQuery+" AND "+KEY_PRODUCTNAME+" LIKE '%"+productName.trim()+"%'";
			}
			if(cellar!=null && !cellar.trim().equals("")){
				appendQuery = appendQuery+" AND "+KEY_CELLAR+" LIKE '%"+cellar.trim()+"%'";
			}
			if(amount!=null && !amount.trim().equals("")){
				appendQuery = appendQuery+" AND CAST("+KEY_AVAILABLE+" as integer ) "+amtCond+" '"+amount.trim()+"'";
			}
			if(price!=null && !price.trim().equals("")){
				appendQuery = appendQuery+" AND CAST("+KEY_PRICE+" as integer ) "+priceCond+" '"+price.trim()+"'";
			}
			if(category!=null && !category.trim().equals("")){
				appendQuery = appendQuery+" AND "+KEY_CATEGORY+" LIKE '"+category.trim()+"'";
			}
			if(tag!=null && !tag.trim().equals("")){
				appendQuery = appendQuery+" AND "+KEY_TAG+" LIKE '%"+tag.trim()+"%'";
			}

			String selectQuery = "SELECT  * FROM " + TABLE_PRODUCT + " WHERE 1=1"+appendQuery+" ORDER BY "+KEY_ID+" DESC LIMIT "+offset+","+ countPerPage;

			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Product product = new Product();
					product.setId(Integer.parseInt(cursor.getString(0)));
					product.setProductCode(cursor.getString(1));
					product.setProductName(cursor.getString(2));
					product.setPrice(cursor.getString(3));
					product.setDescription(cursor.getString(4));
					product.setCategory(cursor.getString(5));
					product.setTag(cursor.getString(6));
					product.setCellar(cursor.getString(7));
					product.setAvailable(cursor.getString(8));
					product.setUpdatedOn(cursor.getString(9));
					// Adding product to list
					productList.add(product);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETALLPRODUCTS", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return product list
		return productList;
	}

	// Deleting single product
	public void deleteProduct(Product product) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			db.delete(TABLE_PRODUCT, KEY_ID + " = ?",
					new String[] { String.valueOf(product.getId()) });
		} catch (Exception e) {
			Log.e("ERROR_DELETEPRODUCT", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}


	/********* PRODUCT IMAGE ********************/
	// Adding new productImage
	public void addProductImage(ProductImage productimage) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, productimage.getId()); // product image id
			values.put(KEY_PRODUCTID, productimage.getProductId()); // product id
			values.put(KEY_PRODUCTIMAGE,productimage.getProductImage()); // ProductImage
			values.put(KEY_UPDATEDON, productimage.getUpdatedOn()); // UpdatedOn

			// Inserting Row
			db.insert(TABLE_PRODUCTIMAGE, null, values);
		} catch (Exception e) {
			Log.e("ERROR_ADDPRODUCTIMAGE", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}

	// Updating single product
	public int updateProductImage(ProductImage productimage) {
		int returnValue = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, productimage.getId()); // product image id
			values.put(KEY_PRODUCTID, productimage.getProductId()); // product id
			values.put(KEY_PRODUCTIMAGE,productimage.getProductImage()); // ProductImage
			values.put(KEY_UPDATEDON, productimage.getUpdatedOn()); // UpdatedOn

			// updating row
			returnValue = db.update(TABLE_PRODUCTIMAGE, values, KEY_ID + " = ?",
					new String[] { String.valueOf(productimage.getId()) });
		} catch (Exception e) {
			Log.e("ERROR_UPDATEPRODUCTIMAGE", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		return returnValue;
	}

	// Getting product Image
	public ProductImage getProductImage(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		ProductImage productImage = null;
		try{
			Cursor cursor = db.query(TABLE_PRODUCTIMAGE, new String[] { KEY_ID,
					KEY_PRODUCTID, KEY_PRODUCTIMAGE,KEY_UPDATEDON }, KEY_ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			productImage = new ProductImage(Integer.parseInt(cursor.getString(0)),
					cursor.getString(1), cursor.getBlob(2),cursor.getString(3));
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETPRODUCTS", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return product Image
		return productImage;
	}

	// Getting ProductImage for a productId
	public List<ProductImage> getProductIamgesByProductId(String productId) {
		List<ProductImage> productImageList = new ArrayList<ProductImage>();
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTIMAGE +" WHERE "+KEY_PRODUCTID+" = '"+productId +"'";

			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					ProductImage productImage = new ProductImage();
					productImage.setId(Integer.parseInt(cursor.getString(0)));
					productImage.setProductId(cursor.getString(1));
					productImage.setProductImage(cursor.getBlob(2));
					productImage.setUpdatedOn(cursor.getString(3));

					// Adding product to list
					productImageList.add(productImage);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETPRODUCTIAMGESBYPRODUCTID", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return product list
		return productImageList;
	}
	
	// Getting ProductImage for a category
		public List<ProductImage> getProductIamgesByCategory(String category) {
			List<ProductImage> productImageList = new ArrayList<ProductImage>();
			SQLiteDatabase db = this.getWritableDatabase();
			try{
				// Select All Query
				String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTIMAGE +" WHERE "+KEY_PRODUCTID+" IN (SELECT "+KEY_ID+" FROM "+TABLE_PRODUCT+" WHERE "+KEY_CATEGORY+" = '"+category +"' )";

				Cursor cursor = db.rawQuery(selectQuery, null);

				// looping through all rows and adding to list
				if (cursor.moveToFirst()) {
					do {
						ProductImage productImage = new ProductImage();
						productImage.setId(Integer.parseInt(cursor.getString(0)));
						productImage.setProductId(cursor.getString(1));
						productImage.setProductImage(cursor.getBlob(2));
						productImage.setUpdatedOn(cursor.getString(3));

						// Adding product to list
						productImageList.add(productImage);
					} while (cursor.moveToNext());
				}
				cursor.close();
			} catch (Exception e) {
				Log.e("ERROR_GETPRODUCTIAMGESBYPRODUCTID", ""+e);
			}
			finally{
				db.close(); // Closing database connection
			}
			// return product list
			return productImageList;
		}
		
		// Getting ProductImage for a tag
				public List<ProductImage> getProductIamgesByTag(String tag) {
					List<ProductImage> productImageList = new ArrayList<ProductImage>();
					SQLiteDatabase db = this.getWritableDatabase();
					try{
						// Select All Query
						String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTIMAGE +" WHERE "+KEY_PRODUCTID+" IN (SELECT "+KEY_ID+" FROM "+TABLE_PRODUCT+" WHERE "+KEY_TAG+" LIKE '%"+tag +"%' )";

						Cursor cursor = db.rawQuery(selectQuery, null);

						// looping through all rows and adding to list
						if (cursor.moveToFirst()) {
							do {
								ProductImage productImage = new ProductImage();
								productImage.setId(Integer.parseInt(cursor.getString(0)));
								productImage.setProductId(cursor.getString(1));
								productImage.setProductImage(cursor.getBlob(2));
								productImage.setUpdatedOn(cursor.getString(3));

								// Adding product to list
								productImageList.add(productImage);
							} while (cursor.moveToNext());
						}
						cursor.close();
					} catch (Exception e) {
						Log.e("ERROR_GETPRODUCTIAMGESBYPRODUCTID", ""+e);
					}
					finally{
						db.close(); // Closing database connection
					}
					// return product list
					return productImageList;
				}

	/********* CATEGORIES ********************/

	// Adding new Category
	public void addCategory(Categories categories) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, categories.getId()); // category id
			values.put(KEY_CATEGORYNAME, categories.getCategoryName()); // category name
			values.put(KEY_UPDATEDON, categories.getUpdatedOn()); // UpdatedOn

			// Inserting Row
			db.insert(TABLE_CATEGORIES, null, values);
		} catch (Exception e) {
			Log.e("ERROR_ADDCATEGORY", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}

	// Updating single Category
	public int updateCategory(Categories categories) {
		int returnValue = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, categories.getId()); // category id
			values.put(KEY_CATEGORYNAME, categories.getCategoryName()); // category name
			values.put(KEY_UPDATEDON, categories.getUpdatedOn()); // UpdatedOn

			// updating row
			returnValue = db.update(TABLE_CATEGORIES, values, KEY_ID + " = ?",
					new String[] { String.valueOf(categories.getId()) });
		} catch (Exception e) {
			Log.e("ERROR_UPDATECATEGORY", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		return returnValue;
	}

	// Getting Category
	public Categories getCategory(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Categories categories = null;
		try{
			Cursor cursor = db.query(TABLE_CATEGORIES, new String[] { KEY_ID,
					KEY_CATEGORYNAME,KEY_UPDATEDON }, KEY_ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			categories = new Categories(Integer.parseInt(cursor.getString(0)),
					cursor.getString(1),cursor.getString(2));
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETCATEGORY", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return Category
		return categories;
	}

	// Getting All Categories
	public List<Categories> getAllCategories() {
		List<Categories> categoryList = new ArrayList<Categories>();
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_CATEGORIES;

			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Categories category = new Categories();
					category.setId(Integer.parseInt(cursor.getString(0)));
					category.setCategoryName(cursor.getString(1));
					category.setUpdatedOn(cursor.getString(2));
					// Adding category to list
					categoryList.add(category);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETALLCATEGORIES", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return user list
		return categoryList;
	}
	
	// Getting Categories that having products
		public List<Categories> getCategoriesHaveProducts() {
			List<Categories> categoryList = new ArrayList<Categories>();
			SQLiteDatabase db = this.getWritableDatabase();
			try{
				// Select All Query
				String selectQuery = "SELECT  * FROM " + TABLE_CATEGORIES +" WHERE "+KEY_ID+" IN ( SELECT "+KEY_CATEGORY+" FROM " + TABLE_PRODUCT +" )";

				Cursor cursor = db.rawQuery(selectQuery, null);

				// looping through all rows and adding to list
				if (cursor.moveToFirst()) {
					do {
						Categories category = new Categories();
						category.setId(Integer.parseInt(cursor.getString(0)));
						category.setCategoryName(cursor.getString(1));
						category.setUpdatedOn(cursor.getString(2));
						// Adding category to list
						categoryList.add(category);
					} while (cursor.moveToNext());
				}
				cursor.close();
			} catch (Exception e) {
				Log.e("ERROR_GETALLCATEGORIES", ""+e);
			}
			finally{
				db.close(); // Closing database connection
			}
			// return user list
			return categoryList;
		}

	/********* TAGS ********************/

	// Adding new tag
	public void addTag(Tags tags) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, tags.getId()); // tag id
			values.put(KEY_TAGNAME, tags.getTagName()); // tag name
			values.put(KEY_UPDATEDON, tags.getUpdatedOn()); // UpdatedOn

			// Inserting Row
			db.insert(TABLE_TAGS, null, values);
		} catch (Exception e) {
			Log.e("ERROR_ADDTAG", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}

	// Updating single tag
	public int updateTag(Tags tags) {
		int returnValue = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, tags.getId()); // tag id
			values.put(KEY_TAGNAME, tags.getTagName()); // tag name
			values.put(KEY_UPDATEDON, tags.getUpdatedOn()); // UpdatedOn

			// updating row
			returnValue = db.update(TABLE_TAGS, values, KEY_ID + " = ?",
					new String[] { String.valueOf(tags.getId()) });
		} catch (Exception e) {
			Log.e("ERROR_UPDATETAG", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		return returnValue;
	}

	// Getting Tag
	public Tags getTag(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Tags tags = null;
		try{
			Cursor cursor = db.query(TABLE_TAGS, new String[] { KEY_ID,
					KEY_TAGNAME,KEY_UPDATEDON }, KEY_ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			tags = new Tags(Integer.parseInt(cursor.getString(0)),
					cursor.getString(1),cursor.getString(2));
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETTAG", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return tag
		return tags;
	}

	// Getting All Tags
	public List<Tags> getAllTags() {
		List<Tags> tagList = new ArrayList<Tags>();
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_TAGS;

			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Tags tag = new Tags();
					tag.setId(Integer.parseInt(cursor.getString(0)));
					tag.setTagName(cursor.getString(1));
					tag.setUpdatedOn(cursor.getString(2));
					// Adding tag to list
					tagList.add(tag);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETALLTAGS", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return user list
		return tagList;
	}

	
	/********* UPDATE LOG ********************/
	// Adding new Serverupdatelog
	public void addServerupdatelog(Serverupdatelog serverupdatelog) {

		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put(KEY_SERVERUPDATEDON, serverupdatelog.getServerUpdatedOn()); // ServerUpdatedOn

			// Inserting Row
			db.insert(TABLE_SERVERUPDATELOG, null, values);
		} catch (Exception e) {
			Log.e("ERROR_ADDSERVERUPDATELOG", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}

	// Getting single Serverupdatelog
	public Serverupdatelog getServerupdatelog(int id) {
		Serverupdatelog serverupdatelog = null;
		SQLiteDatabase db = this.getReadableDatabase();
		try {
			Cursor cursor = db.query(TABLE_SERVERUPDATELOG, new String[] { KEY_ID,
					KEY_SERVERUPDATEDON}, KEY_ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			serverupdatelog = new Serverupdatelog(Integer.parseInt(cursor.getString(0)),
					cursor.getString(1));
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GETSERVERUPDATELOG", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return user
		return serverupdatelog;
	}

	//get last update
	public List<Serverupdatelog> checkLastUpdate(){
		List<Serverupdatelog> updateList = new ArrayList<Serverupdatelog>();
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			// Select All Query

			String selectQuery = "SELECT  * FROM " + TABLE_SERVERUPDATELOG + " WHERE "+KEY_ID+" = (SELECT MAX("+KEY_ID+") FROM "+TABLE_SERVERUPDATELOG+")";

			Cursor cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Serverupdatelog serverupdatelog = new Serverupdatelog();
					serverupdatelog.setId(Integer.parseInt(cursor.getString(0)));
					serverupdatelog.setServerUpdatedOn(cursor.getString(1));
					// Adding user to list
					updateList.add(serverupdatelog);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
			return updateList;
		}
		catch (SQLException e) {
			Log.e("ERROR_CHECKLASTUPDATE", ""+e);
			return null;
		}
	}

	//get all update
	public List<Serverupdatelog> getAllUpdate(){
		List<Serverupdatelog> updateList = new ArrayList<Serverupdatelog>();
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			// Select All Query

			String selectQuery = "SELECT  * FROM " + TABLE_SERVERUPDATELOG;

			Cursor cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Serverupdatelog serverupdatelog = new Serverupdatelog();
					serverupdatelog.setId(Integer.parseInt(cursor.getString(0)));
					serverupdatelog.setServerUpdatedOn(cursor.getString(1));
					// Adding user to list
					updateList.add(serverupdatelog);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
			return updateList;
		}
		catch (SQLException e) {
			Log.e("ERROR_GETALLUPDATE", ""+e);
			return null;
		}
	}

}

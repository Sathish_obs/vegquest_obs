package com.obs.vegquest.activity;


import java.net.URLEncoder;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject; 
import com.obs.vegquest.dbhandler.DbHandler;
import com.obs.vegquest.domain.Restaurant;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class SplashScreen extends Activity implements CommonConstants{

	Context context;
	Thread dataload; 
	TextView status;
	TextView splashLabel;
	CommonUtils utils = new CommonUtils();
	boolean updateFirstTime = false;
	ProgressBar progressBar;
	private static String TAG = SplashScreen.class.getName();
	private static long SLEEP_TIME = 5;    // Sleep for some time

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);    // Removes notification bar 
		setContentView(R.layout.splashscreen);
		context = this;
		splashLabel =(TextView) findViewById(R.id.splashLabel);
		splashLabel.setText(Html.fromHtml(SPLASH_LABEL));
		CommonUtils.justifyText(((TextView)findViewById(R.id.splashLabel)), 200f);
		progressBar =(ProgressBar)findViewById(R.id.progressBar1);

		progressBar.setVisibility(View.GONE);
		IntentLauncher launcher = new IntentLauncher();
		launcher.start();

	}

	//redirect
	public void redirect(){
		//redirection
		Intent i = new Intent(context, Home.class);
		startActivity(i);
	}




	public void showSettingsAlert(String title,String message){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(SplashScreen.this);

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
				startActivity(intent);
			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}


	private class IntentLauncher extends Thread {
		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */
		public void run() {
			try {
				// Sleeping
				Thread.sleep(SLEEP_TIME*1000);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}

			// Start main activity
			Intent intent = new Intent(getApplicationContext(), Home.class);
			startActivity(intent);
			finish();
		}
	} 

}



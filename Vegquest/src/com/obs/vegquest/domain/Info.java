package com.obs.vegquest.domain;

public class Info {

	/**
	 * Name: Info
	 * Use : This class is used for transfer the Information
	 * @author John
	 *
	 */
	private String id;
	private String tbl_user_id;
	private String tbl_hotel_id;
	private String title;
	private String desc;
	private String time; 


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTbl_user_id() {
		return tbl_user_id;
	}
	public void setTbl_user_id(String tbl_user_id) {
		this.tbl_user_id = tbl_user_id;
	}
	public String getTbl_hotel_id() {
		return tbl_hotel_id;
	}
	public void setTbl_hotel_id(String tbl_hotel_id) {
		this.tbl_hotel_id = tbl_hotel_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
 }

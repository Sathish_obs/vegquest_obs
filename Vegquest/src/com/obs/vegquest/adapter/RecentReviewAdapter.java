package com.obs.vegquest.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.obs.vegquest.activity.R;

public class RecentReviewAdapter   extends BaseAdapter{

	private Context context;
	private ArrayList<HashMap<String, String>> data;
	private static LayoutInflater inflater=null;


	public RecentReviewAdapter(Context context, ArrayList<HashMap<String, String>> d) {
		this.context = context;
		data=d;
		inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}



	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi=convertView;
		if(convertView==null)
			vi = inflater.inflate(R.layout.recentlistreview, null);

		TextView title = (TextView)vi.findViewById(R.id.username); // username
		TextView city = (TextView)vi.findViewById(R.id.city);
		TextView hotelName = (TextView)vi.findViewById(R.id.hotelName);
		TextView artist = (TextView)vi.findViewById(R.id.review); // review		    
		ImageView favorite = (ImageView)vi.findViewById(R.id.favourites); // review
		HashMap<String, String> review = new HashMap<String, String>();
		review = data.get(position);
		favorite.setVisibility(View.GONE);
		// Setting all values in listview
		if(review.get("city")!=null){
			city.setText(Html.fromHtml("<b>City :</b>")+review.get("city"));
		}else{
			city.setText("");
		}
		if(review.get("username")!=null){
			title.setText("User:"+review.get("username"));
		}
		artist.setText(Html.fromHtml("<b>Review : ")+review.get("review"));
		hotelName.setText(Html.fromHtml("<b>Restaurant :")+review.get("resturantName"));

		return vi;
	}

}

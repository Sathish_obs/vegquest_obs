package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.obs.vegquest.adapter.ReviewAdapter;
import com.obs.vegquest.domain.Restaurant;
import com.obs.vegquest.domain.Review;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewReview extends Activity implements CommonConstants {

	//UI controls declaration
	ImageView aboutus;
	Button backButton;
	ListView detailList;
	TextView hotelName;
	TextView noResult;
	static ProgressDialog progress = null;
	Context context;
	
	//global variables declaration	
	ReviewAdapter adapter;
	int hotelId;
	String hotelname;
	String URL ;
	String arr[];
	ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
	String userId;
	String from;
	String criteria;
	String location;
	String name;
	String miles;
	String category;
	public  static  List<Review> reviewList = new ArrayList<Review>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.viewreview);
		// UI controls instantiation
		URL = getResources().getString(R.string.baseURL);
		backButton =(Button)findViewById(R.id.backButton) ;
		aboutus =(ImageView)findViewById(R.id.aboutus) ;
		detailList =(ListView)findViewById(R.id.reviewList) ;
		hotelName =(TextView)findViewById(R.id.hotelName);
		noResult =(TextView)findViewById(R.id.noResult);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		context =this;
		Bundle b = getIntent().getExtras();
		try {
			if(b!=null){
				hotelId = b.getInt("hotelId");
				hotelname =b.getString("hotelName");
				hotelId = b.getInt("hotelId");
				from  =b.getString("from");
				if(CommonUtils.isNetworkAvailable(getBaseContext())){
					hotelName.setText(""+hotelname);
					progress.show();
					Thread dataload = new Thread(){
						@Override
						public void run() {
							//Garbage collection
							System.gc();	 
							if(getIntent().getExtras()!=null){ //Webservice call
								JSONArray hotelArray = CommonUtils.readJsonArray(URL+"listreviews?hotelId="+URLEncoder.encode(""+hotelId));
								try {
									if(hotelArray!=null){
										for (int i = 0; i < hotelArray.length(); i++) {

											JSONObject objects = hotelArray.getJSONObject(i);
											Log.i("loadUser"+i, objects.toString());
											String userid 		= (!objects.isNull("UserId"))?(String)objects.get("UserId"):"";
											String username		= (!objects.isNull("User Name"))?(String)objects.get("User Name"):"";
											String resturantId = (!objects.isNull("RestaurantId"))?(String)objects.get("RestaurantId"):"";
											String msg 	= (!objects.isNull("Message"))?(String)objects.get("Message"):"";
											String city 	= (!objects.isNull("City"))?(String)objects.get("City"):"";
											String reviewId 	= (!objects.isNull("ReviewId"))?(String)objects.get("ReviewId"):"";
											HashMap<String, String> map = new HashMap<String, String>();
											map.put("username",username);
											map.put("review",msg);
											//		 					map.put("city",city);
											map.put("userId",userid);
											map.put("reviewId",reviewId);
											map.put("hotelId",resturantId);
											songsList.add(map);	
										} 
										sendMessage(songsList);

									} else{

										sendMessage(new ArrayList());
									}
								}catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
								}	

							}	progress.dismiss();
							interrupt();
						}
					};
					dataload.start();	
				}else{
					Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
				}

			}	 
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		// Click event for single list row
		detailList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {


				HashMap<String, String> v = songsList.get(position);
				if(v!=null){
					if(v.containsKey("username")){
						Intent intent = getIntent();
						intent.putExtra("username", v.get("username"));
						intent.putExtra("review", v.get("review"));
						intent.putExtra("hotelId", v.get("hotelId"));
						intent.putExtra("hotelName", hotelname);
						intent.putExtra("userId", v.get("userId"));
						intent.putExtra("reviewId", Integer.parseInt(v.get("reviewId")));
						intent.setClass(getApplicationContext(),UpdateReview.class);
						startActivity(intent);

					}
				}
			}
		});		

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = getIntent();
				intent.putExtra("hotelName", hotelname);
				intent.putExtra("hotelId", hotelId);
				intent.putExtra("from", from);
				intent.setClass(getApplicationContext(), HotelDetail.class);
				startActivity(intent);
			}
		});

	}	  



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_view_review, menu);
		return true;
	}
	
	//handler call

	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
		 
			try{
				Bundle b = msg.getData();
				ArrayList   sonList = b.getParcelableArrayList("songsList");
				if(sonList.size()==0){ // check the list size is empty or not
					noResult.setVisibility(View.VISIBLE);
					detailList.setVisibility(View.GONE);
				}else{
					detailList.setVisibility(View.VISIBLE); 
					noResult.setVisibility(View.INVISIBLE);
					adapter = new ReviewAdapter(context,songsList);
					detailList.setAdapter(adapter);
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}	

		}

	};


	public void sendMessage(ArrayList arrayList){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putParcelableArrayList("songsList", arrayList);
		msg.setData(b);
		handler.sendMessage(msg);
	}  


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}
}

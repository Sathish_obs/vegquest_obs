package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.obs.vegquest.adapter.ReviewAdapter;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class WriteReview extends Activity implements CommonConstants,OnRatingBarChangeListener {

	//UI controls declaration
	ImageView aboutus;
	Button backButton;
	Button submitButton;
	TextView hotelName;
	EditText review;
	static ProgressDialog progress = null;
	RatingBar ratingBar;

	//global variables declation
	String URL ;
	int  hotelId  ;
	String userId ;
	String hotelname;
	String from ;
	String criteria ;
	String location;
	String name;
	String miles;
	String category;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.writereview);
		// UI controls instantiation
		URL = getResources().getString(R.string.baseURL);
		aboutus = (ImageView)findViewById(R.id.aboutus);
		backButton =(Button)findViewById(R.id.backButton);
		submitButton =(Button)findViewById(R.id.submit);
		review =(EditText)findViewById(R.id.review);
		hotelName =(TextView)findViewById(R.id.hotelName);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		ratingBar = (RatingBar)findViewById(R.id.rating);

		Bundle b = getIntent().getExtras();
		/**
		 * Get the information from Shared prefernce
		 * It is mainly used for storing Object in session 
		 */
		SharedPreferences settings = getSharedPreferences("userObject", 0);
		if(settings!=null) {
			if(settings.getString("userId", null)!=null || settings.getBoolean("isLoggedin", Boolean.FALSE) ) {

				userId = settings.getString("userId",null);
			}
		}		
		if(b!=null) {

			hotelId=getIntent().getExtras().getInt("hotelId");

			hotelname = b.getString("hotelName");
			hotelName.setText(""+hotelname);
			from = b.getString("from");
		}
		// aboutus button action
		ratingBar.setOnRatingBarChangeListener(this);
		aboutus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(getBaseContext(), AboutUs.class);
				myIntent.putExtra("from", "reviewpage");
				myIntent.putExtra("hotelId",getIntent().getExtras().getInt("hotelId"));
				startActivity(myIntent);
			}
		});
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				Intent myIntent = new Intent(getBaseContext(), HotelDetail.class);
				myIntent.putExtra("hotelId",getIntent().getExtras().getInt("hotelId"));
				myIntent.putExtra("from", from);
				startActivity(myIntent);
			}
		});


		submitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				if(CommonUtils.checkEmpty(review)){

					if(CommonUtils.isNetworkAvailable(getBaseContext())){
						progress.show();
						Thread dataload = new Thread(){
							@Override
							public void run() {
								//Garbage collection
								System.gc();	 
								JSONArray jsonObject = CommonUtils.readJsonArray(URL+"addreview?userId="+URLEncoder.encode(userId)+"&hotelId="+URLEncoder.encode(""+hotelId)+"&review="+URLEncoder.encode(""+review.getText().toString().trim()));
								try {
									if(jsonObject.get(0)!=null){
										if(jsonObject.getJSONObject(0).getString("status").equals("true")){
											sendMessage(""+Html.fromHtml(REVIEW_SUCCESS));
										} 
									}
								}catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace() ;
								}	
								progress.dismiss();
								interrupt();
							}
						};
						dataload.start();	      

					}	else{

						Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
					}


				}else{
					AlertDialog.Builder altDialog= new AlertDialog.Builder(WriteReview.this);
					altDialog.setMessage(""+Html.fromHtml(VALIDATION_LABEL));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show();
				}
			}

		});	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_review, menu);
		return true;
	}

	//handler call for webservice
	Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();
				Toast.makeText(getApplicationContext(),b.getString("msg"), Toast.LENGTH_SHORT).show();
				Intent intent = new Intent();
				intent.putExtra("hotelId",getIntent().getExtras().getInt("hotelId"));
				intent.putExtra("hotelName",getIntent().getExtras().getString("hotelName"));
				intent.putExtra("from",getIntent().getExtras().getString("from"));
				intent.setClass(getApplicationContext(),HotelDetail.class);
				startActivity(intent);

			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}	
		}
	};

	public void sendMessage(String message){
		Message msg = new Message();
		Bundle b = new Bundle();

		b.putString("msg", message);
		msg.setData(b);
		handler.sendMessage(msg);
	}

	@Override
	public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
		// TODO Auto-generated method stub
		try{
			// TODO Auto-generated method stub
			if(CommonUtils.isNetworkAvailable(getBaseContext())){
				if(userId!=null){
					float rat = ratingBar.getRating();
					//webservice call 
					String registerUrl = getResources().getString(R.string.baseURL)+"ratehotel?hotelId="+URLEncoder.encode(""+hotelId)+"&userId="+userId+"&ratecount="+URLEncoder.encode(""+rat);
					JSONArray jsonObject1 = CommonUtils.readJsonArray(registerUrl);
					if(jsonObject1.get(0)!=null){
						if(jsonObject1.getJSONObject(0).getString("status").equals("true")){

							Toast.makeText(getApplicationContext(),"Rating posted",Toast.LENGTH_LONG).show();
						}
					}	   

				}else{
					AlertDialog.Builder altDialog= new AlertDialog.Builder(WriteReview.this);
					altDialog.setMessage(""+Html.fromHtml(""+Html.fromHtml(""+Html.fromHtml(LOGIN_LABEL))));
					altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							Intent intent = getIntent();
							intent.setClass(getApplicationContext(),Login.class);
							startActivity(intent);
						}
					});
					altDialog.setNegativeButton(CANCEL_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							dialog.cancel();
						}
					});

					altDialog.show();
				}

			}else{

				Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}		 
	}  

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

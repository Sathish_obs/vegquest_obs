package com.obs.vegquest.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.obs.vegquest.activity.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class ReviewAdapter  extends BaseAdapter{
	private Context context;
	private ArrayList<HashMap<String, String>> data;
	private static LayoutInflater inflater=null;


	public ReviewAdapter(Context context, ArrayList<HashMap<String, String>> d) {
		this.context = context;
		data=d;
		inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}



	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi=convertView;
		if(convertView==null)
			vi = inflater.inflate(R.layout.listreview, null);

		TextView title = (TextView)vi.findViewById(R.id.username); // title
		TextView city = (TextView)vi.findViewById(R.id.city);
		TextView artist = (TextView)vi.findViewById(R.id.review); // artist name

		HashMap<String, String> song = new HashMap<String, String>();
		song = data.get(position);

		// Setting all values in listview
		if(song.get("city")!=null){
			city.setText("City:"+song.get("city"));
		}else{
			city.setText("");
		}
		city.setVisibility(View.GONE);
		title.setText(song.get("username"));
		artist.setText("Review:"+song.get("review"));


		return vi;
	}


}

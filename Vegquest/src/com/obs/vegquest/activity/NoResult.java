package com.obs.vegquest.activity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class NoResult extends Activity {

	Button back;
	ImageView backButton1;
	ImageView aboutus;
	String from;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.noresult);
		back =(Button)findViewById(R.id.backButton);
		backButton1 =(ImageView)findViewById(R.id.backButton1);
		aboutus =(ImageView)findViewById(R.id.aboutus);
		Intent intent = getIntent();

		Bundle b =intent.getExtras();
		if(b!=null){
			from = b.getString("from");
		}	  
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();

				//if(from.equals("basicsearch"))
				intent.setClass(getApplicationContext(), Search.class);
				//else if(from.equals("advancesearch"))
				//intent.setClass(getApplicationContext(), Settings.class);
				startActivity(intent);

			}
		});

		backButton1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();

				//if(from.equals("basicsearch"))
				intent.setClass(getApplicationContext(), Search.class);
				//else if(from.equals("advancesearch"))
				//intent.setClass(getApplicationContext(), Settings.class);
				startActivity(intent);

			}
		});

		aboutus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent =getIntent();
				intent.setClass(getBaseContext(),AboutUs.class);
				intent.putExtra("from", "noresult");
				startActivity(intent);

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_no_result, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Search.class);
		startActivity(intent);
	}

}

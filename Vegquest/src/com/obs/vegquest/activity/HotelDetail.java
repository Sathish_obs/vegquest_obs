package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.UserDataHandler;

import com.obs.vegquest.domain.Info;
import com.obs.vegquest.domain.Restaurant;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class HotelDetail extends Activity implements CommonConstants{

	ImageView aboutus;
	Button backButton;
	Button viewReview;
	Button writeReview;
	TextView hotelName;
	TextView phoneNo;
	TextView address;
	String URL ;
	RatingBar rating;
	ListView detailList;
	TextView  point;
	TextView  viewmap;
	String hotelname;
	ImageView phoneIcon;
	public  static  ArrayList<Restaurant> resList = new ArrayList<Restaurant>();
	public  static  ArrayList<Restaurant> resList1 = new ArrayList<Restaurant>();
	ArrayList<Info>  infoList = new ArrayList<Info>();
	String userId  ;
	String userName ;
	int hotelId ;
	String from ="";
	String criteria;
	ImageView  favourite;
	int favHotel ;
	String location;
	String name;
	String miles;
	String category;
	double latitude;
	double longitude;
	String  destinationAddress ;
	static   String viewMapUrl = "https://maps.google.com/maps?saddr=";
	static ProgressDialog progress = null;
	ImageView infoBar;
	TextView infoMsg;
	ImageView infoImage;
	TextView viewWebSite;
	ListView infoListView;
	String hoursValue;
	ScrollView scrollView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.hdetail);
		URL = getResources().getString(R.string.baseURL);
		aboutus = (ImageView)findViewById(R.id.aboutus);
		backButton =(Button)findViewById(R.id.backButton);
		hotelName= (TextView)findViewById(R.id.hotelName);
		phoneNo= (TextView)findViewById(R.id.phone);
		address= (TextView)findViewById(R.id.address);
		detailList =(ListView)findViewById(R.id.listItem) ;
		viewReview =(Button)findViewById(R.id.viewReview) ;
		writeReview =(Button)findViewById(R.id.writereview) ;
		rating =(RatingBar)findViewById(R.id.rating);
		point =(TextView)findViewById(R.id.point);
		viewmap =(TextView)findViewById(R.id.viewmap);
		favourite =(ImageView)findViewById(R.id.favourites);
		infoBar = (ImageView)findViewById(R.id.imageView4);
		infoMsg = (TextView)findViewById(R.id.textView4);
		infoImage = (ImageView)findViewById(R.id.imageView2);
		infoListView = (ListView)findViewById(R.id.listItem);
		phoneIcon  = (ImageView)findViewById(R.id.phoneIcon);
		/*Pattern pattern = Pattern.compile("View Direction");
		Linkify.addLinks(viewmap, pattern, "viewmap://");*/
		Linkify.addLinks(viewmap,Linkify.ALL);
		viewmap.setText(Html.fromHtml("<a href='#'>View Direction</a>"));
		viewWebSite = (TextView)findViewById(R.id.viewWebsite);
		scrollView = (ScrollView)findViewById(R.id.my_scrollview);
		scrollView.setScrollbarFadingEnabled(false);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		View headerView =  ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.test, null, false);
		detailList.addHeaderView(headerView); 



		try {

			Bundle b = getIntent().getExtras();
			SharedPreferences settings = getSharedPreferences("userObject", 0);
			if(settings!=null) {
				if(settings.getString("userId", null)!=null || settings.getString("userName", null)!=null && settings.getBoolean("isLoggedin", Boolean.FALSE) ) {
					userName = settings.getString("userName",null);
					userId = settings.getString("userId",null);
				}
			} 		

			if(CommonUtils.isEmpty(userId))
				rating.setIsIndicator(true);
			else
				rating.setIsIndicator(false);
			if(b!=null){

				if(b.getInt("hotelId")!=0){
					hotelId = b.getInt("hotelId");
				}
				if(b.getString("from")!=null){
					from = b.getString("from");
				} 
			}  

			favourite.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					// TODO Auto-generated method stub
					try{
						if(CommonUtils.isNetworkAvailable(getBaseContext())){
							if(userId!=null && !userId.equals("0")) {
								progress.show();
								Thread dataload = new Thread(){
									@Override
									public void run() {
										//Garbage collection
										System.gc();
										String registerUrl = getResources().getString(R.string.baseURL)+"favouritehotel?userId="+URLEncoder.encode(userId)+"&hotelId="+URLEncoder.encode(""+hotelId)+"&fav="+favHotel;
										JSONArray jsonObject1 = CommonUtils.readJsonArray(registerUrl);
										favoritePressed(jsonObject1);
										progress.dismiss();
										interrupt();
									}
								};
								dataload.start();

							} else{

								AlertDialog.Builder altDialog= new AlertDialog.Builder(HotelDetail.this);
								altDialog.setMessage(""+Html.fromHtml(LOGIN_LABEL));
								altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub

										Intent intent = getIntent();
										intent.setClass(getApplicationContext(),Login.class);
										startActivity(intent);

									}
								});


								altDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub


										dialog.cancel();
									}
								});

								altDialog.show();
							}
						}	 else {
							Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
						}

					}catch(Exception e){
						e.printStackTrace();
					}
				}  	
			});   

			phoneNo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					AlertDialog.Builder altDialog= new AlertDialog.Builder(HotelDetail.this);
					altDialog.setMessage(phoneNo.getText().toString().replaceAll("-", ""));
					altDialog.setPositiveButton("Call", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							String phone_no= phoneNo.getText().toString().replaceAll("-", "");
							Intent callIntent = new Intent(Intent.ACTION_CALL);
							callIntent.setData(Uri.parse("tel:"+phone_no));
							callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
							startActivity(callIntent);

						}
					});

					altDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							dialog.cancel();
						}
					});

					altDialog.show();

				}
			});  

			if(CommonUtils.isNetworkAvailable(getBaseContext())){
				if(getIntent().getExtras()!=null){

					String detailUrl = URL+"detail?criteria="+URLEncoder.encode(""+hotelId);
					if(!CommonUtils.isEmpty(userId))
						detailUrl+="&userId="+userId;
					JSONArray hotelArray = CommonUtils.readJsonArray(detailUrl);
					resList = CommonUtils.loadResturant(hotelArray);
					if(resList!=null){
						Restaurant res = (Restaurant)resList.get(0);
						hotelname = res.getName();

						if(res.getHours()!=null){
							hoursValue = res.getHours();
							Info info = new Info();
							info.setTitle("Hours:");
							info.setDesc(hoursValue);
							infoList.add(info);
						} 

						if(!CommonUtils.isEmpty(res.getWebSite())){
							if(res.getWebSite().toLowerCase().startsWith("http://") || res.getWebSite().toLowerCase().startsWith("www"))
								viewWebSite.setText(res.getWebSite().toLowerCase());
							else 
								viewWebSite.setText("http://"+res.getWebSite().toLowerCase());

						}else{
							viewWebSite.setText("");
						}
						if(res.getPhoneno()!=null && !res.getPhoneno().equals("")){
							phoneIcon.setVisibility(View.VISIBLE);
							phoneNo.setVisibility(View.VISIBLE);
							phoneNo.setText( res.getPhoneno());

						}else {
							phoneIcon.setVisibility(View.INVISIBLE);
							phoneNo.setVisibility(View.INVISIBLE);
						}

						hotelName.setText(Html.fromHtml("<u>"+hotelname+"</u>"));

						address.setText(res.getAddress()+", "+res.getCity()+", "+res.getState());
						rating.setRating(res.getRating());
						point.setText(""+Math.round(res.getRating()));
						if(res.isFavourite()==1){
							favourite.setBackgroundResource(R.drawable.unfavorite);
							favHotel = 0;
						}  
						else {
							favourite.setBackgroundResource(R.drawable.favorite);
							favHotel =1;
						} 	
						//destinationAddress =res.getCity()+" "+res.getState();
						destinationAddress =res.getName()+","+res.getAddress()+","+res.getCity()+","+res.getState();
					}


					resList1.clear();
					JSONArray infoArray = CommonUtils.readJsonArray(URL+"viewinfo?hotelId="+URLEncoder.encode(""+hotelId));
					if(infoArray!=null){
						for (int i = 0; i < infoArray.length(); i++) {

							JSONObject objects = infoArray.getJSONObject(i);

							String id 		= (!objects.isNull("InfoId"))?(String)objects.get("InfoId"):"";
							String RestaurantId = (!objects.isNull("RestaurantId"))?(String)objects.get("RestaurantId"):"";
							String title 	= (!objects.isNull("Tittle"))?(String)objects.get("Tittle"):"";
							String desc 	= (!objects.isNull("Description"))?(String)objects.get("Description"):"";
							String time 	= (!objects.isNull("Time"))?(String)objects.get("Time"):""; 

							Info info = new Info();
							info.setId(id);
							info.setTbl_hotel_id(id);
							info.setTitle(title);
							info.setDesc(desc);
							info.setTime(time);
							infoList.add(info);
						}		
					}


					for(int i =0;i<infoList.size();i++){
						resList1.add(new Restaurant(infoList.get(i).getTime(),infoList.get(i).getTitle(),infoList.get(i).getDesc()));   
					}

					if(infoList.size()==0 ){
						infoBar.setVisibility(View.INVISIBLE);
						infoMsg.setVisibility(View.INVISIBLE);
						infoImage.setVisibility(View.INVISIBLE);
						infoListView.setVisibility(View.GONE);
					}
					RestaurantDetailAdaper orderAdapter = new RestaurantDetailAdaper(this,R.layout.row,resList1);
					detailList.setAdapter(orderAdapter);

				} 		 

			}else {
				Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL),Toast.LENGTH_SHORT).show();

			}			 	 

			viewmap.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					loadUrlCallforWeb("viewMap");

				}
			});

			viewWebSite.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					loadUrlCallforWeb(viewWebSite.getText().toString());

				}
			});


		}catch (Exception e) {

			e.printStackTrace();
		}	 


		// aboutus button action
		aboutus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(getBaseContext(), AboutUs.class);
				myIntent.putExtra("from", "detailpage");
				myIntent.putExtra("hotelId",hotelId);
				startActivity(myIntent);
			}
		});
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				resList.clear();
				resList1.clear();

				Intent myIntent  = new Intent();
				if(from.equals("favouritesList")){
					myIntent.setClass(getApplicationContext(), FavouritesList.class);
				}else{
					myIntent.setClass(getApplicationContext(), HotelList.class);
					myIntent.putExtra("from", from);
					myIntent.putExtra("criteria", criteria);
				}	


				startActivity(myIntent);
			}
		});

		// review button action
		viewReview.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				Intent myIntent = new Intent(getBaseContext(), ViewReview.class);
				myIntent.putExtra("hotelId",hotelId);
				myIntent.putExtra("hotelName",hotelname);
				myIntent.putExtra("from", from);
				startActivity(myIntent); 
			}
		});


		rating.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {

				try{
					// TODO Auto-generated method stub
					if(CommonUtils.isNetworkAvailable(getBaseContext())){
						if(userId!=null){

							String registerUrl = getResources().getString(R.string.baseURL)+"ratehotel?hotelId="+URLEncoder.encode(""+resList.get(0).getId())+"&userId="+userId+"&ratecount="+URLEncoder.encode(""+rating);

							JSONArray jsonObject1 = CommonUtils.readJsonArray(registerUrl);

							if(jsonObject1.get(0)!=null){
								if(jsonObject1.getJSONObject(0).getString("status").equals("true")){

									Toast.makeText(getApplicationContext(),"Rating posted",Toast.LENGTH_LONG).show();
									point.setText(""+Math.round(rating));
								}
							}	   

						}else{
							AlertDialog.Builder altDialog= new AlertDialog.Builder(HotelDetail.this);
							altDialog.setMessage(LOGIN_LABEL);
							altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub

									Intent intent = getIntent();
									intent.setClass(getApplicationContext(),Login.class);
									startActivity(intent);

								}
							});
							altDialog.setNegativeButton(CANCEL_LABEL, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {

									dialog.cancel();
								}
							});

							altDialog.show();
						}



					} else{

						Toast.makeText(getApplicationContext(),NETWORK_UNAVAILABLE_LABEL,Toast.LENGTH_LONG).show();
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}    

			}
		}); 

	 writeReview.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				if(userId!=null) {
					Intent myIntent = new Intent(getBaseContext(), WriteReview.class);
					myIntent.putExtra("hotelId",hotelId);
					myIntent.putExtra("from", from);
					myIntent.putExtra("hotelName",hotelname);
					myIntent.putExtra("from", from);
					startActivity(myIntent);
				}else{
					AlertDialog.Builder altDialog= new AlertDialog.Builder(HotelDetail.this);
					altDialog.setMessage(Html.fromHtml(REVIEW_LABEL));
					altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							Intent intent = getIntent();
							intent.setClass(getApplicationContext(),Login.class);
							startActivity(intent);

						}
					});
					altDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub


							dialog.cancel();
						}
					});

					altDialog.show();
				}
			}
		});
		  

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_hotel_detail, menu);
		return true;
	}

	private class RestaurantDetailAdaper extends ArrayAdapter<Restaurant> {

		Context context; 
		int layoutResourceId;    
		ArrayList<Restaurant> data = null;


		public RestaurantDetailAdaper(Context context,int layoutResourceId, ArrayList<Restaurant> resturants ) {
			super(context, layoutResourceId, resturants);
			this.layoutResourceId = layoutResourceId;
			this.context = context;
			this.data = resturants;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row, null);
			}
			Restaurant res = data.get(position);
			if (res != null) {
				TextView tt = (TextView) v.findViewById(R.id.topText);
				TextView bt = (TextView) v.findViewById(R.id.bottomText);

				if (tt != null) {
					tt.setText(Html.fromHtml("<u>"+res.getName()+"</u>"));                            }
				if(bt != null){
					bt.setText(res.getAddress());

				}

			}
			return v;
		}
	}



	/** Sets up the WebView object and loads the URL of the page **/
	private void loadUrlCallforWeb(String from){

		try{
			if(from.equals("viewMap")){
				GPSTracker gps = new GPSTracker(HotelDetail.this);
				if(gps.canGetLocation()){

					latitude = gps.getLatitude();
					longitude = gps.getLongitude();
				} 	


				Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
				if(geo!=null){
					List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
					if (addresses.isEmpty()) {
						Toast.makeText(getApplicationContext(), "Loading", Toast.LENGTH_LONG).show();
					}
					else {
						if (addresses.size() > 0) {
							Address add= addresses.get(0);
							add.getAddressLine(0);
							viewMapUrl+=""+add.getAddressLine(0)+"&daddr="+destinationAddress+"&z=1";
							startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(viewMapUrl)));
							//
						}
					}
				}	
			}else{
				viewMapUrl = from;
			}


		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}


	public void favoritePressed(JSONArray  jsonObject1){
		try {
			if(jsonObject1!=null){
				if(jsonObject1.getJSONObject(0).getString("status").equals("true")){

					if(favHotel ==0) {
						favHotel =1;
						sendMessage(""+Html.fromHtml(UNFAVOURITE_LABEL));

					}    
					else {
						favHotel =0; 
						sendMessage(""+Html.fromHtml(FAVOURITE_LABEL));

					}

				}
			} 
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
	}

	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();

				if(favHotel ==0)  
					favourite.setBackgroundResource(R.drawable.unfavorite);
				else
					favourite.setBackgroundResource(R.drawable.favorite);

				Toast.makeText(getApplicationContext(),  b.getString("message") ,Toast.LENGTH_LONG).show();


			}catch (Exception e) {
				// TODO: handle exception
			}	

		}

	};

	public void sendMessage(String message){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		msg.setData(b);
		handler.sendMessage(msg);
	} 

	/*	@Override
	public void onRatingChanged(RatingBar ratingBar, float rating,
			boolean fromUser) {
		try{
			// TODO Auto-generated method stub
			if(CommonUtils.isNetworkAvailable(getBaseContext())){
				if(userId!=null){

					String registerUrl = getResources().getString(R.string.baseURL)+"ratehotel?hotelId="+URLEncoder.encode(""+resList.get(0).getId())+"&userId="+userId+"&ratecount="+URLEncoder.encode(""+rating);

					JSONArray jsonObject1 = CommonUtils.readJsonArray(registerUrl);

					if(jsonObject1.get(0)!=null){
						if(jsonObject1.getJSONObject(0).getString("status").equals("true")){
							point.setText(""+Math.round(rating));
							Toast.makeText(getApplicationContext(),"Rating posted",Toast.LENGTH_LONG).show();

						}
					}	   

				}else{
					AlertDialog.Builder altDialog= new AlertDialog.Builder(HotelDetail.this);
					altDialog.setMessage(LOGIN_LABEL);
					altDialog.setPositiveButton(YES_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							Intent intent = getIntent();
							intent.setClass(getApplicationContext(),Login.class);
							startActivity(intent);

						}
					});
					altDialog.setNegativeButton(CANCEL_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub


							dialog.cancel();
						}
					});

					altDialog.show();
				}



			} else{

				Toast.makeText(getApplicationContext(),NETWORK_UNAVAILABLE_LABEL,Toast.LENGTH_LONG).show();
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}    


	}	 */


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}


}

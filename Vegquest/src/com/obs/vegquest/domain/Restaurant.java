package com.obs.vegquest.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List; 
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class Restaurant  implements Parcelable {

	/**
	 * Name: Restaurant
	 * Use : This class is used for transfer the  Restaurant Information
	 * @author John
	 *
	 */
	private int id ;
	private String name;
	private String state;
	private String city;
	private String address;
	private String zipcode;
	private String type;
	private float rating;
	private String desc;
	private String phoneno;
	private int favourite;
	private String lastUpdatedDate;
	private String time;
	private String webSite ;
	private String hours ;
	ArrayList<Info> infoList = new ArrayList<Info>(); 

	public Restaurant() {

	}

	public Restaurant(int id, String name,String state,String city,String address,String zipcode, String type,
			float rating, String desc, String phoneno) {
		super();
		this.id = id;
		this.name = name;
		this.state =state;
		this.city=city;
		this.address = address;
		this.zipcode=zipcode;
		this.type = type;
		this.rating = rating;
		this.desc = desc;
		this.phoneno = phoneno;
	}
	public Restaurant(String time,String name,String address){

		this.name=name;
		this.address=address;
		this.time = time;
	}

	public Restaurant(int id,String city,String state,String lastUpdatedDate){

		this.id=id;
		this.city=city;
		this.state=state;
		this.lastUpdatedDate=lastUpdatedDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public String getDistance() {
		return desc;
	}

	public void setDistance(String distance) {
		this.desc = distance;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeString(state);
		dest.writeString(city);
		dest.writeString(address);
		dest.writeString(zipcode);
		dest.writeString(type);
		dest.writeFloat(rating);
		dest.writeString(desc);
		dest.writeString(phoneno);
		dest.writeInt(favourite);
		dest.writeString(lastUpdatedDate);
		dest.writeString(time);
		dest.writeString(webSite);
		dest.writeString(hours);
	}

	public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {

		@Override
		public Restaurant createFromParcel(Parcel source) {

			// TODO Auto-generated method stub
			return   new Restaurant(source);
		}

		@Override
		public Restaurant[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Restaurant[size];
		}
	};
	public Restaurant(Parcel source) {

		id = source.readInt();
		name = source.readString();
		state = source.readString();
		city   = source.readString();
		address =source.readString();
		zipcode =source.readString();
		type = source.readString();
		rating = source.readFloat();
		desc = source.readString();
		phoneno =source.readString();
		favourite =source.readInt();
		lastUpdatedDate =source.readString();
		time = source.readString();
		webSite = source.readString();
		hours = source.readString();
	}

	public ArrayList<Info> getInfoList() {
		return infoList;
	}

	public void setInfoList(ArrayList<Info> infoList) {
		this.infoList = infoList;
	}

	public int isFavourite() {
		return favourite;
	}

	public void setFavourite(int favourite) {
		this.favourite = favourite;
	}

	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	} 



}

package com.obs.vegquest.adapter;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import com.obs.vegquest.activity.HotelDetail;
import com.obs.vegquest.activity.R;
import com.obs.vegquest.domain.Restaurant;
import com.obs.vegquest.utils.CommonUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


public class HotelAdapter  extends  BaseAdapter{


	private Context context;  
	private List<Restaurant> hotelList = new ArrayList<Restaurant>();
	String userId;
	public HotelAdapter(Context context,List<Restaurant> list) {  
		this.context = context; 
		hotelList = list;
		SharedPreferences settings = context.getSharedPreferences("userObject", 0);
		if(settings!=null) {
			if(settings.getString("userId", null)!=null || settings.getString("userName", null)!=null && settings.getBoolean("isLoggedin", Boolean.FALSE) ) {
				userId  = settings.getString("userId", null);

			}
		}	


	}  

	@Override
	public int getCount() {  
		return hotelList.size();  
	}  

	@Override
	public Object getItem(int position) {  
		return hotelList.get(position);  
	}  

	@Override
	public long getItemId(int position) {  
		return position;  
	}  

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {  
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView;

		if (convertView == null) {

			gridView = new View(context);

			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.gridtemplate, null);



		} else {
			gridView = convertView;
		}

		// set value into textview
		TextView textView = (TextView) gridView.findViewById(R.id.hotelname);
		textView.setText(hotelList.get(position).getName());
		textView.setContentDescription(String.valueOf(hotelList.get(position).getId()));
		textView.setFocusable(false);
		textView.setFocusableInTouchMode(false);
		TextView address = (TextView) gridView.findViewById(R.id.address);
		address.setText(hotelList.get(position).getAddress());
		address.setContentDescription(String.valueOf(hotelList.get(position).getId()));
		address.setFocusable(false);
		address.setFocusableInTouchMode(false);
		TextView type = (TextView) gridView.findViewById(R.id.type);
		type.setText(hotelList.get(position).getType());
		type.setContentDescription(String.valueOf(hotelList.get(position).getId()));
		type.setFocusable(false);
		type.setFocusableInTouchMode(false);
		ImageView rating =(ImageView)gridView.findViewById(R.id.rating);
		rating.setClickable(false);
		rating.setFocusable(false);
		rating.setFocusableInTouchMode(false);
		TextView distance = (TextView) gridView.findViewById(R.id.distance);
		distance.setText(hotelList.get(position).getDistance());
		distance.setContentDescription(String.valueOf(hotelList.get(position).getId()));
		distance.setFocusable(false);
		distance.setFocusableInTouchMode(false);
		final ImageView phoneIcon = (ImageView) gridView.findViewById(R.id.phoneIcon);
		final TextView phoneno = (TextView) gridView.findViewById(R.id.phone);
		phoneno.setContentDescription(String.valueOf(hotelList.get(position).getId()));
		phoneno.setFocusable(false);
		phoneno.setFocusableInTouchMode(false);

		if(hotelList.get(position).getPhoneno()!=null && !hotelList.get(position).getPhoneno().equals("")) {
			phoneIcon.setVisibility(View.VISIBLE);
			phoneno.setVisibility(View.VISIBLE);
			phoneno.setText(hotelList.get(position).getPhoneno());

		}else{
			phoneIcon.setVisibility(View.INVISIBLE);
			phoneno.setVisibility(View.INVISIBLE);
		}

		if( Math.round(hotelList.get(position).getRating())==1.0)
			rating.setBackgroundResource(R.drawable.star01);
		else if( Math.round(hotelList.get(position).getRating())==2.0)
			rating.setBackgroundResource(R.drawable.star02);
		else if( Math.round(hotelList.get(position).getRating())==3.0)
			rating.setBackgroundResource(R.drawable.star03);
		else if( Math.round(hotelList.get(position).getRating())==4.0)
			rating.setBackgroundResource(R.drawable.star04);
		else if( Math.round(hotelList.get(position).getRating())==5.0)
			rating.setBackgroundResource(R.drawable.star05);
		else 
			rating.setBackgroundResource(R.drawable.no_enable);
		phoneno.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				String phone = phoneno.getText().toString().replaceAll("-", "");
				AlertDialog.Builder altDialog= new AlertDialog.Builder(context);
				altDialog.setMessage(phone);

				altDialog.setPositiveButton("Call", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						String phone_no=  phoneno.getText().toString().replaceAll("-", "");
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:"+phone_no));
						callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
						context.startActivity(callIntent);

					}
				});

				altDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub


						dialog.cancel();
					}
				});

				altDialog.show();
			}
		});  


		return gridView;
	}  
}

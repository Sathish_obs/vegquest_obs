package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import com.obs.vegquest.adapter.FavouriteAdapter;
import com.obs.vegquest.adapter.RecentReviewAdapter;
import com.obs.vegquest.adapter.ReviewAdapter;
import com.obs.vegquest.domain.Review;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ReviewList extends Activity implements CommonConstants {

	ImageView aboutus;
	Button backButton;
	ListView detailList;
	ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
	int hotelId;
	String hotelname;
	String URL ;
	String arr[];
	RecentReviewAdapter adapter;
	TextView hotelName;
	String userId;
	String from;
	String city;
	GPSTracker gps;
	TextView noresult;
	Context context;
	String state;
	String miles;
	public  static  ArrayList<Review> reviewList = new ArrayList<Review>();
	static ProgressDialog progress = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.viewreview);
		URL = getResources().getString(R.string.baseURL);
		backButton =(Button)findViewById(R.id.backButton) ;
		aboutus =(ImageView)findViewById(R.id.aboutus) ;
		detailList =(ListView)findViewById(R.id.reviewList) ;
		hotelName =(TextView)findViewById(R.id.hotelName);
		noresult =(TextView)findViewById(R.id.noResult);
		gps = new GPSTracker(ReviewList.this);
		context = this;
		Intent intent = new Intent();
		intent.putExtra("from", "reviewList");
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));

		/*b.putString("from", "reviewList");*/
		try {

			SharedPreferences settings = getSharedPreferences("Settings", 0);
			SharedPreferences.Editor editor = settings.edit();
			if(settings!=null) {
				if(settings.getString("city", null)!=null || settings.getString("state", null)!=null) {
					city  = settings.getString("city", null);
					state  = settings.getString("state", null);
					miles  = settings.getString("miles", null);
				}
			}   		


			Bundle b =getIntent().getExtras();
			if(b!=null){
				hotelId = b.getInt("hotelId");
				/*userId  =b.getString("userId");*/
				hotelname =b.getString("hotelName");			
				from  =b.getString("from");

			}
			hotelName.setText("Recent Reviews");
			if(CommonUtils.isNetworkAvailable(getBaseContext())){


				progress.show();
				Thread dataload = new Thread(){
					@Override
					public void run() {
						//Garbage collection
						System.gc(); 

						reviewList.clear();
						JSONArray hotelArray = CommonUtils.readJsonArray(URL+"recentreview?city="+URLEncoder.encode(""+city)+"&miles="+URLEncoder.encode(""+miles));
						if(hotelArray!=null){
							reviewList =  loadReview(hotelArray);
							sendMessage(reviewList);
						}else{
							sendMessage(reviewList);
						}
						progress.dismiss();
						interrupt();
					}
				};
				dataload.start();

			}	else{
				Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
			}

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}



		// Click event for single list row
		detailList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {


				HashMap<String, String> v = songsList.get(position);
				if(v!=null){
					if(v.containsKey("username")){

						Intent intent = getIntent();
						intent.putExtra("username", v.get("username"));
						intent.putExtra("review", v.get("review"));
						intent.putExtra("hotelId", v.get("hotelId"));
						intent.putExtra("hotelName", v.get("resturantName"));
						intent.putExtra("from", "reviewlist");
						intent.setClass(getApplicationContext(),UpdateReview.class);
						startActivity(intent);

					}
				}


			}
		});		

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = getIntent();

				intent.putExtra("hotelName", hotelname);
				intent.putExtra("hotelId", hotelId);
				/*intent.putExtra("userId", userId);*/
				intent.putExtra("from", from);
				intent.setClass(getApplicationContext(), Home.class);
				startActivity(intent);

			}
		});

	}	  



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_view_review, menu);
		return true;
	}

	//to handle msg which is send from thread. and update to the textview
	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();
				ArrayList statusMsg = b.getParcelableArrayList("restaurant");
				adapter = new RecentReviewAdapter(context,statusMsg);  
				if(reviewList!=null){
					detailList.setAdapter(adapter);
					if(reviewList.size()==0){
						noresult.setVisibility(View.VISIBLE);
						detailList.setVisibility(View.INVISIBLE);
					} else{
						noresult.setVisibility(View.INVISIBLE);
						detailList.setVisibility(View.VISIBLE);
					}
				}else{
					noresult.setVisibility(View.VISIBLE);
					detailList.setVisibility(View.INVISIBLE);

				}
			}catch (Exception e) {
				// TODO: handle exception
			}	

		}

	};

	public void sendMessage(ArrayList resList){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putParcelableArrayList("restaurant", resList);
		msg.setData(b);
		handler.sendMessage(msg);
	}

	public ArrayList loadReview(JSONArray hotelArray){
		try {
			if(hotelArray!=null){
				for (int i = 0; i < hotelArray.length(); i++) {

					JSONObject objects = hotelArray.getJSONObject(i);
					Log.i("loadUser"+i, objects.toString());

					String userid 		= (!objects.isNull("UserId"))?(String)objects.get("UserId"):"";
					String username		= (!objects.isNull("User Name"))?(String)objects.get("User Name"):"";
					String resturantId = (!objects.isNull("RestaurantId"))?(String)objects.get("RestaurantId"):"";
					String resturantName = (!objects.isNull("Restaurant Name"))?(String)objects.get("Restaurant Name"):"";
					String msg 	= (!objects.isNull("Message"))?(String)objects.get("Message"):"";
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("username",username);
					map.put("userId",userid);
					map.put("review",msg);
					map.put("city",city);
					map.put("resturantName",resturantName);
					map.put("hotelId",resturantId);
					songsList.add(map);	


				} 


			}		  
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}		  
		return  songsList;	  

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

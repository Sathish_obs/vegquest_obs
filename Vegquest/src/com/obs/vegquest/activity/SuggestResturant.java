package com.obs.vegquest.activity;

import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SuggestResturant extends Activity  implements CommonConstants {


	EditText hotelName;
	EditText location;
	EditText type;
	EditText website;
	EditText phone;
	Button  backButton;
	Button  submit;
	String URL = null ;
	static ProgressDialog progress = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.suggest);
		backButton = (Button)findViewById(R.id.backButton);
		submit = (Button)findViewById(R.id.submit);
		hotelName = (EditText)findViewById(R.id.hotelname);
		location = (EditText)findViewById(R.id.location);
		type = (EditText)findViewById(R.id.type);
		website = (EditText)findViewById(R.id.website);
		phone = (EditText)findViewById(R.id.phone);

		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(getBaseContext(), Home.class);
				myIntent.putExtra("from", "searchpage");
				startActivity(myIntent);

			}
		});	

		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				if(!CommonUtils.checkEmpty(hotelName) ||
						!CommonUtils.checkEmpty(location)){

					AlertDialog.Builder altDialog= new AlertDialog.Builder(SuggestResturant.this);
					altDialog.setMessage(Html.fromHtml(SUGGESTVALIDATION_LABEL));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show(); 
				}else{

					if(CommonUtils.isNetworkAvailable(getBaseContext())){
						progress.show();
						Thread dataload = new Thread(){
							@Override
							public void run() {
								//Garbage collection
								System.gc();	 
								URL = getResources().getString(R.string.baseURL)+"suggesthotel?hotelname="+URLEncoder.encode(hotelName.getText().toString().trim())+"&location="+URLEncoder.encode(location.getText().toString().trim())+"&type="+URLEncoder.encode(type.getText().toString().trim())+"&website="+URLEncoder.encode(website.getText().toString().trim())+"&phone="+URLEncoder.encode(phone.getText().toString().trim());
								JSONArray jsonObject =CommonUtils.readJsonArray(URL);
								try {
									if(jsonObject!=null){
										if(jsonObject.getJSONObject(0).getString("status")!=null || jsonObject.getJSONObject(0).getString("status").equals("true")){
											sendMessage(""+Html.fromHtml(SUGGEST_SUCCESS));
										}

									}
								}catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
								}
								progress.dismiss();
								interrupt();
							}
						};
						dataload.start();	
					}else{
						Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
					}


				}
			} 
		});		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_suggest_resturant, menu);
		return true;
	}


	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();

				AlertDialog.Builder altDialog= new AlertDialog.Builder(SuggestResturant.this);
				altDialog.setMessage(b.getString("message"));
				altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.cancel();
						Intent intent = new Intent();
						intent.setClass(getApplicationContext(),Home.class);
						startActivity(intent);
					}
				});
				altDialog.show();

			}catch (Exception e) {
				// TODO: handle exception
			}	

		}

	};


	public void sendMessage(String message){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		msg.setData(b);
		handler.sendMessage(msg);
	}  

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}
}

package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.ads.f;
import com.obs.vegquest.adapter.FavouriteAdapter;
import com.obs.vegquest.dbhandler.DbHandler;
import com.obs.vegquest.domain.Country;
import com.obs.vegquest.domain.Restaurant;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;
import android.widget.AdapterView.OnItemClickListener;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Settings extends Activity implements CommonConstants {

	Button backButton;
	Button search;
	EditText hotelName;
	AutoCompleteTextView country;
	AutoCompleteTextView state;
	AutoCompleteTextView city;
	EditText zipCode;
	Spinner  distance;
	Spinner  category;
	List<String> distanceList ;
	List<String> categoryList ;
	List<String> restaurants ;
	static ProgressDialog progress = null;
	ImageView showCategory;
	GPSTracker gps;
	String currentCity;
	String currentState;
	String currentZipCode;
	TextView changePass;
	String userId;
	String URL;
	ImageView findLocation;
	Context context;
	GPSTracker gpsTracker;
	ArrayList countryList = null;
	boolean countryLoaded;
	boolean stateLoaded;
	boolean cityLoaded;
	String currentCountry;
	static ArrayList<Country> stateList = new ArrayList<Country>();
	static ArrayList<Country> cityList = new ArrayList<Country>();
	String selectedStateId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings);
		gps = new GPSTracker(Settings.this);
		URL =  getResources().getString(R.string.baseURL);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		backButton =(Button)findViewById(R.id.backButton);
		search =(Button)findViewById(R.id.searchButton);
		hotelName =(EditText)findViewById(R.id.name);
		country =(AutoCompleteTextView)findViewById(R.id.country);
		state =(AutoCompleteTextView)findViewById(R.id.state);
		city =(AutoCompleteTextView)findViewById(R.id.city);

		zipCode =(EditText)findViewById(R.id.zipCode);
		distance =(Spinner)findViewById(R.id.distance);
		//distance.setEnabled(Boolean.FALSE);
		category =(Spinner)findViewById(R.id.category);
		showCategory =(ImageView)findViewById(R.id.showCategory);
		hotelName.setVisibility(View.INVISIBLE);
		changePass = (TextView)findViewById(R.id.changepass);
		changePass.setText(Html.fromHtml("<u>Change password</u>"));
		search.setBackgroundResource(R.drawable.done_btn);
		//search.setText("DONE");
		findLocation =(ImageView)findViewById(R.id.findLocation);
		gpsTracker = new GPSTracker(this);
		context = this;
		countryList =  new ArrayList<String>();
		distanceList = new ArrayList<String>();
		categoryList = new ArrayList<String>();
		restaurants = new ArrayList<String>() ;


		state.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					state.setText("");
					city.setText("");
				}
			}
		});

		//search.setText("Done");
		SharedPreferences settingsUserObject = getSharedPreferences("userObject", 0);
		if(settingsUserObject!=null) {
			if(settingsUserObject.getString("userId", null)!=null || settingsUserObject.getString("userName", null)!=null && settingsUserObject.getBoolean("isLoggedin", Boolean.FALSE) ) {
				userId  = settingsUserObject.getString("userId", null);

				changePass.setVisibility(View.VISIBLE);
			}else{
				changePass.setVisibility(View.GONE);
			}
		}	

		changePass.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder altDialog= new AlertDialog.Builder(Settings.this);
				altDialog.setTitle("Change Password");
				LayoutInflater inflater = (LayoutInflater) v.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
				View view = inflater.inflate(R.layout.login_dialog,null);
				altDialog.setView(view);
				final EditText oldPassword = (EditText)view.findViewById(R.id.oldPassword);
				final EditText newPassword = (EditText)view.findViewById(R.id.newPassword);
				final EditText confirmPassword = (EditText)view.findViewById(R.id.confirmPassword);
				altDialog.setNeutralButton("Submit", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(final DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						try {

							if(!CommonUtils.checkEmpty(oldPassword) || !CommonUtils.checkEmpty(newPassword)  || !CommonUtils.checkEmpty(confirmPassword)){
								Toast.makeText(getApplicationContext(), ""+Html.fromHtml(VALIDATIONALL_LABEL), Toast.LENGTH_LONG).show();
							}else if(!newPassword.getText().toString().trim().equals(confirmPassword.getText().toString().trim())){
								Toast.makeText(getApplicationContext(), ""+Html.fromHtml("Password is not match"), Toast.LENGTH_LONG).show();
								confirmPassword.setFocusable(true);

							}else if(CommonUtils.isNetworkAvailable(getApplicationContext())) {
								progress.show();
								final String changepass =URL +"changepassword?oldpassword="+URLEncoder.encode(oldPassword.getText().toString().trim())+"&confirmpassword="+URLEncoder.encode(confirmPassword.getText().toString().trim())+"&userId="+URLEncoder.encode(userId);
								Thread dataload = new Thread(){
									@Override
									public void run() {
										//Garbage collection
										System.gc(); 
										JSONArray jsonObject1 = CommonUtils.readJsonArray(changepass);
										try {
											if(jsonObject1!=null && jsonObject1.get(0)!=null){
												if(jsonObject1.getJSONObject(0).getString("status").equals("true")){
													// Toast.makeText(getApplicationContext(),PASSWORD_CHANGESUCCESS,Toast.LENGTH_LONG).show();
													sendMessage(""+Html.fromHtml(PASSWORD_CHANGESUCCESS),null);
													//dialog.cancel();
												}else{
													sendMessage(jsonObject1.getJSONObject(0).getString("reason"),null);   	
													//Toast.makeText(getApplicationContext(),jsonObject1.getJSONObject(0).getString("reason"),Toast.LENGTH_LONG).show();
													// dialog.cancel();
												}
											}else{
												//sendMessage("Failure");   
												//dialog.cancel();
											}
										}catch (Exception e) {
											// TODO: handle exception
											e.printStackTrace();
										}

										progress.dismiss();
										interrupt();
									}
								};
								dataload.start();

							}else{

								Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();	 
							}
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace() ;
						} 			

					}
				});

				altDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.cancel();
					}
				});
				altDialog.show(); 

			}
		});
		//showCategory.setText(Html.fromHtml("<a href='#'>---</a>"));
		for(int i=0;i<DISTANCE.length;i++){
			distanceList.add(DISTANCE[i]);
		}
		for(int j=0;j<CATEGORY.length;j++){
			categoryList.add(CATEGORY[j]);
		}

		city.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View view, int keyCode, KeyEvent event) {
				if (event != null && keyCode == KeyEvent.KEYCODE_ENTER) {
					if(CommonUtils.isNetworkAvailable(getBaseContext())){
						settingsSaved();
					}else{
						Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();

					}
					return true;
				}else{
					return false;
				}	
			}
		});  

		findLocation.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(CommonUtils.isNetworkAvailable(context)) {

					if(gpsTracker.canGetLocation()){
						try {  
							Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
							List<Address> addresses = geo.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
							if (addresses.size() > 0) {
								//MAP_URL+=""+addresses.get(0).getLocality()+" "+addresses.get(0).getAdminArea()+"&daddr="+destinationAddress+"&z=1";
								city.setText(addresses.get(0).getLocality());
								state.setText(addresses.get(0).getAdminArea());
							}
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}else{

					}
				}else{
					Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
				}

			}
		});



		if(CommonUtils.isNetworkAvailable(this)){
			//progress.setMessage(getResources().getString(R.string.loadingCountry));  
			progress.setMessage(getResources().getString(R.string.loadingStates));
			progress.show();
			Thread dataload = new Thread(){
				@Override
				public void run() {

					try {

						final String loadState =URL +"loadState";
						JSONArray stateArray = CommonUtils.readJsonArray(loadState);
						if(stateArray!=null){
							for (int i = 0; i < stateArray.length(); i++) {

								JSONObject objects = stateArray.getJSONObject(i);
								Log.i("loadUser"+i, objects.toString());

								String id 		= (!objects.isNull("id"))?(String)objects.get("id"):"";
								String name		= (!objects.isNull("name"))?(String)objects.get("name"):"";
								stateList.add(new Country(id,name));
							} 
							stateLoaded = Boolean.TRUE;
							sendMessage("",stateList);
						}

					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					} 	
					progress.dismiss();
					interrupt();
				}		

			};
			dataload.start();

		}else{
			Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
		}

		//restaurants.addAll(stateList);


		state.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(final  AdapterView<?> arg0, View arg1,final int arg2, long arg3) {
				// TODO Auto-generated method stub
				selectedStateId =  ((Country)arg0.getAdapter().getItem(arg2)).getId();
				loadCountry(selectedStateId);

			}
		});


		//StateList
		/*ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,stateList);
		state.setAdapter(adapter); */
		//Distance List
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, distanceList);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		//category List
		ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, categoryList);
		dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		distance.setAdapter(dataAdapter);
		distance.setSelection(2,true);
		category.setSelection(0);
		category.setAdapter(dataAdapter1);
		SharedPreferences settings = getSharedPreferences("Settings", 0);
		SharedPreferences.Editor editor = settings.edit();
		try {
			if(settings!=null) {

				if(settings.getString("city", null)!=null && settings.getString("state", null)!=null){
					if(!settings.getString("city", null).equals("") ||  !settings.getString("state", null).equals("")){ 

						currentCountry  = settings.getString("country", null);
						currentCity  = settings.getString("city", null);
						currentState  = settings.getString("state", null);
						selectedStateId  = settings.getString("stateId", null);
						currentZipCode  = settings.getString("zipCode", null);
						String miles =settings.getString("miles", null);
						String selectedCategory =settings.getString("category", null);
						country.setText(currentCountry);
						city.setText(currentCity);
						state.setText(currentState);
						zipCode.setText(currentZipCode);

						if(miles.equals("1"))
							distance.setSelection(0,true);
						else if(miles.equals("5"))
							distance.setSelection(1,true);
						else if(miles.equals("20"))
							distance.setSelection(2,true);
						else if(miles.equals("50"))
							distance.setSelection(3,true);
						else if(miles.equals("60"))
							distance.setSelection(4,true);

						if(selectedCategory.equals("VGN"))
							category.setSelection(0,true);
						else if(selectedCategory.equals("VGT"))
							category.setSelection(1,true);
						else if(selectedCategory.equals("VGT, VGN-F"))
							category.setSelection(2,true);
						else if(selectedCategory.equals("VGT, VGN-F, RAW"))
							category.setSelection(3,true);

						if(CommonUtils.checkEmpty(state)){
							loadCountry(selectedStateId);
						}


					}else{
						if(gps.canGetLocation()){

							double latitude = gps.getLatitude();
							double longitude = gps.getLongitude();

							try {
								Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
								List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);

								if (addresses!=null &&  addresses.isEmpty()) {
									Toast.makeText(getApplicationContext(), "Loading", Toast.LENGTH_LONG).show();
								}
								else {
									if (addresses.size() > 0) {
										currentCity = addresses.get(0).getLocality();
										currentState = addresses.get(0).getAdminArea();
										currentCountry = addresses.get(0).getCountryName();
										editor.putString("country",currentCountry);
										editor.putString("city",currentCity);
										editor.putString("state",currentState);
										editor.putString("miles",distance.getSelectedItem().toString());
										editor.putString("category", category.getSelectedItem().toString());
										editor.putString("zipCode", zipCode.getText().toString().trim());
										editor.commit();
										//  Toast.makeText(getApplicationContext(),"", Toast.LENGTH_LONG).show();
										city.setText(currentCity);
										state.setText(currentState);
										country.setText(currentCountry);

									}
								}
							}
							catch (Exception e) {
								e.printStackTrace(); // getFromLocation() may sometimes fail
							}
						}	

					}
				}else if(gps.canGetLocation()){

					double latitude = gps.getLatitude();
					double longitude = gps.getLongitude();

					try {
						Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
						List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);

						if (addresses!=null &&  addresses.isEmpty()) {
							Toast.makeText(getApplicationContext(), "Loading", Toast.LENGTH_LONG).show();
						}
						else {
							if (addresses.size() > 0) {
								currentCity = addresses.get(0).getLocality();
								currentState = addresses.get(0).getAdminArea();
								currentCountry = addresses.get(0).getCountryName();
								editor.putString("country",currentCountry);
								editor.putString("city",currentCity);
								editor.putString("state",currentState);
								editor.putString("miles",distance.getSelectedItem().toString());
								editor.putString("category", category.getSelectedItem().toString());
								editor.putString("zipCode", zipCode.getText().toString().trim());
								editor.commit();
								//  Toast.makeText(getApplicationContext(),"", Toast.LENGTH_LONG).show();
								city.setText(currentCity);
								state.setText(currentState);
								country.setText(currentCountry);

							}
						}
					}
					catch (Exception e) {
						e.printStackTrace(); // getFromLocation() may sometimes fail
					}

				}else{
					gps.showSettingsAlert();
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	 
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = getIntent();
				i.setClass(getApplicationContext(), Home.class);
				startActivity(i);

			}
		});

		showCategory.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder altDialog= new AlertDialog.Builder(Settings.this);
				altDialog.setTitle(Html.fromHtml(DESC_LABEL));
				altDialog.setMessage(Html.fromHtml(DESCLIST_LABEL));
				altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.cancel();
					}
				});
				altDialog.show(); 

			}
		});


		search.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(CommonUtils.isNetworkAvailable(getBaseContext())){
					settingsSaved();
				}else{
					Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
				}
			}	

		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_advance_search, menu);
		return true;
	}


	public void settingsSaved(){

		SharedPreferences settings = getSharedPreferences("Settings", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("country", country.getText().toString().trim());
		editor.putString("state", state.getText().toString().trim());
		editor.putString("stateId", selectedStateId);
		editor.putString("city", city.getText().toString().trim());		
		editor.putString("zipCode", zipCode.getText().toString().trim());
		editor.putString("criteria", null);
		editor.putString("miles", distance.getSelectedItem().toString());
		editor.putString("category", category.getSelectedItem().toString());

		editor.commit();
		final String  requestURL =URL+"mypreference?country="+URLEncoder.encode(country.getText().toString().trim())+"&state="+URLEncoder.encode(state.getText().toString().trim())+
				"&city="+URLEncoder.encode(city.getText().toString().trim())+"&zipCode="+URLEncoder.encode(zipCode.getText().toString().trim())+
				"&miles="+URLEncoder.encode(distance.getSelectedItem().toString())+"&category="+ category.getSelectedItem().toString()+"&userId="+userId;
		if(CommonUtils.isNetworkAvailable(getApplicationContext())){
			progress.show();
			progress.setMessage(getResources().getString(R.string.loading));
			Thread dataload = new Thread(){
				@Override
				public void run() {
					//Garbage collection
					System.gc(); 
					try{

						JSONArray jsonObject1 = CommonUtils.readJsonArray(requestURL);

						if(jsonObject1!=null && jsonObject1.get(0)!=null){
							if(jsonObject1.getJSONObject(0).getString("status").equals("true")){
								// Toast.makeText(getApplicationContext(),PASSWORD_CHANGESUCCESS,Toast.LENGTH_LONG).show();
								sendMessage("Settings Saved",null);
								//dialog.cancel();
							}else{
								sendMessage(jsonObject1.getJSONObject(0).getString("reason"),null);
							}
						}	
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

					progress.dismiss();
					interrupt();
				}
			};
			dataload.start();

		}else{
			Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();
		}
		//

	}


	//to handle msg which is send from thread. and update to the textview
	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			Bundle b = msg.getData();
			ArrayList resultList = b.getStringArrayList("resultList");
			boolean flag = b.getBoolean("flag");

			if(countryLoaded && resultList!=null ){

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,resultList);
				country.setAdapter(adapter); 
				countryLoaded =Boolean.FALSE;

			} if(stateLoaded && resultList!=null){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,resultList);
				state.setAdapter(adapter);
				stateLoaded =Boolean.FALSE;

			} if(cityLoaded && resultList!=null){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,resultList);
				city.setAdapter(adapter); 
				cityLoaded =Boolean.FALSE;
			} 
			String statusMsg = b.getString("message");
			if(statusMsg!=null && !statusMsg.equals(""))
				Toast.makeText(getApplicationContext(),statusMsg,Toast.LENGTH_LONG).show();

		}

	};


	public void sendMessage(String message,ArrayList countryList){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		b.putStringArrayList("resultList", countryList);
		msg.setData(b);
		handler.sendMessage(msg);
	} 


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);
		return true;
	}


	public void loadCountry(final String stateId){
		if(CommonUtils.isNetworkAvailable(getApplicationContext())){ 
			progress.setMessage(getResources().getString(R.string.loadingCities));  
			progress.show();
			Thread dataload = new Thread(){
				@Override
				public void run() {

					try{
						final String loadStates =URL +"loadCity?stateId="+URLEncoder.encode(stateId);
						JSONArray cityArray = CommonUtils.readJsonArray(loadStates);
						if(cityArray!=null){
							for (int i = 0; i < cityArray.length(); i++) {

								JSONObject objects = cityArray.getJSONObject(i);
								Log.i("loadUser"+i, objects.toString());

								String id 		= (!objects.isNull("id"))?(String)objects.get("id"):"";
								String name		= (!objects.isNull("name"))?(String)objects.get("name"):""; 
								cityList.add(new Country(id,name));
							}
							cityLoaded =Boolean.TRUE;
							sendMessage("",cityList);
						}	 
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}	 	

					//} 

					progress.dismiss();
					interrupt();
					//cityList=null;
				}
			};	
			dataload.start();
		}else{
			Toast.makeText(getApplicationContext(),""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL), Toast.LENGTH_LONG).show();		 
		}


	}


}

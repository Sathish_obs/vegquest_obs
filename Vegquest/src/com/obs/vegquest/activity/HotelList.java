package com.obs.vegquest.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import com.obs.vegquest.adapter.HotelAdapter;
import com.obs.vegquest.domain.Restaurant;
import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ClipData.Item;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class HotelList extends Activity  implements CommonConstants  {

	//UI controls declaration
	Button back;
	GridView hotelListView;
	ImageView aboutusButton;
	private HotelAdapter ia; 
	Context context;
	TextView numberofResults;
	
	//global variables declaration
	String URL ;
	String userId;
	String userName;
	String from ;
	String criteria;
	String location;
	String name;
	String miles;
	String category;
	String city;
	String state;
	String zipCode;
	static ProgressDialog progress = null;
	ArrayList arrayList ;
	public  static ArrayList<Restaurant> hotelList ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.hotellist);
		
		// UI controls instantiation
		URL = getResources().getString(R.string.baseURL);
		back = (Button)findViewById(R.id.backButton);
		hotelListView = (GridView)findViewById(R.id.categorySearchContentlayout);
		aboutusButton = (ImageView)findViewById(R.id.aboutus);
		context = this;
		numberofResults = (TextView)findViewById(R.id.name);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading)); 
		arrayList  = new ArrayList() ;
		hotelList = new ArrayList<Restaurant>() ;
		try {
			if(CommonUtils.isNetworkAvailable(getBaseContext())){ 
				
				/**
				 * Get the information from Shared prefernce
				 * It is mainly used for storing Object in session 
				 */
				SharedPreferences settings = getSharedPreferences("userObject", 0);
				if(settings!=null) {
					if(settings.getString("userId", null)!=null || settings.getString("userName", null)!=null && settings.getBoolean("isLoggedin", Boolean.FALSE) ) {
						userName = settings.getString("userName",null);
						userId = settings.getString("userId",null);
					}
				}	
				if(getIntent().getExtras()!=null){ //Get the value from the bundle
					Bundle bundle =getIntent().getExtras();
					from = bundle.getString("from");
					SharedPreferences searchSettings = getSharedPreferences("searchObject", 0);
					if(searchSettings!=null) {

						if(searchSettings.getString("city", null)!=null && searchSettings.getString("state", null)!=null) {
							city =searchSettings.getString("city", null);
							state =searchSettings.getString("state", null);
							zipCode =searchSettings.getString("zipCode", null);
							name = searchSettings.getString("name", null);
							miles = searchSettings.getString("miles", null);
							category = searchSettings.getString("category", null);
						} 		
					}	 


					progress.show();
					Thread dataload = new Thread(){
						@Override
						public void run() {
							//Garbage collection
							System.gc();
							//redirection
							if(city!=null && state!=null ){
								if(city.equals("") && state.equals(""))
									city =zipCode;
								else
									city +=", "+state;
							}
							//webservice call
							arrayList = advanceSearchCall(city,name,miles,category);
							if(arrayList!=null)
								sendMessage(arrayList,arrayList.size());
							progress.dismiss();
							interrupt();
						}
					};
					dataload.start();

				} 
			}else {
				Toast.makeText(getApplicationContext(), ""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL),Toast.LENGTH_SHORT).show();

			}

			OnItemClickListener itemClickListener = new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view, int arg1,
						long arg2) {
					Intent intent = getIntent();
					intent.putExtra("hotelId", hotelList.get(arg1).getId());
					intent.putExtra("from", from);
					intent.setClass(getApplicationContext(), HotelDetail.class);
					startActivity(intent);	

				}
			};
			hotelListView.setOnItemClickListener(itemClickListener);

			// start button action
			back.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					hotelList =null;
					Intent intent = getIntent();
					intent.setClass(getApplicationContext(), Search.class);
					startActivity(intent);	
				}
			});


			// aboutus button action
			aboutusButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent myIntent = new Intent(getBaseContext(), AboutUs.class);
					myIntent.putExtra("from", "listingpage");
					myIntent.putExtra("items",hotelList);
					startActivity(myIntent);

				}
			});	 
		}catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_hotel_list, menu);
		return true;
	}


	public ArrayList basicSearchCall(){

		Intent intent = getIntent();
		 
		try { 
			JSONArray hotelArray = CommonUtils.readJsonArray(URL+"search?criteria="+URLEncoder.encode(criteria));
			hotelList=  CommonUtils.loadResturant(hotelArray);
			if(hotelList.size()==0) {
				intent.setClass(getApplicationContext(), NoResult.class);
				startActivity(intent);
			} 


		}catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
		return hotelList; 
	}

	public ArrayList advanceSearchCall(String location,String  hotelname,String miles,String category){

		try { 
			String advSearchUrl = getResources().getString(R.string.baseURL)+"advancesearch?location="+URLEncoder.encode(location)+"&name="+URLEncoder.encode(hotelname)
					+"&miles="+URLEncoder.encode(miles)+"&category="+URLEncoder.encode(category);
			JSONArray jsonObject1 = CommonUtils.readJsonArray(advSearchUrl);
			Intent intent = getIntent();
			hotelList = CommonUtils.loadResturant(jsonObject1);
			if(hotelList.size()==0) {
				intent.setClass(getApplicationContext(), NoResult.class);
				startActivity(intent);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{

		}
		return hotelList; 
	}

	//to handle msg which is send from thread. and update to the textview
	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();
				ArrayList statusMsg = b.getParcelableArrayList("restaurant");
				int count = b.getInt("count");
				ia = new HotelAdapter(context,statusMsg);  
				if(hotelListView!=null){
					hotelListView.setAdapter(ia);

				}else{

				}
				numberofResults.setText("Total found ("+count+")");
			}catch (Exception e) {
				// TODO: handle exception
			}
		}

	};

	public void sendMessage(ArrayList resList,int count){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putParcelableArrayList("restaurant", resList);
		b.putInt("count",count );
		msg.setData(b);
		handler.sendMessage(msg);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

package com.obs.vegquest.activity;

import java.net.URLEncoder;

import org.json.JSONArray;

import com.obs.vegquest.utils.CommonConstants;
import com.obs.vegquest.utils.CommonUtils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ForgotPassword extends Activity implements CommonConstants {

	//UI controls declaration
	Button back;
	EditText email;
	Button submit;
	static ProgressDialog progress = null;
	//global  variables
	String msg;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.forgotpassword);
		// UI controls instantiation
		back =(Button)findViewById(R.id.backButton);
		email=(EditText)findViewById(R.id.email);
		submit =(Button)findViewById(R.id.submit);
		progress =  new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		// searchButton action
		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				if(CommonUtils.checkEmpty(email) ){ // validation for check the email
					if(!CommonUtils.isValidEmail(email.getText().toString())){
						AlertDialog.Builder altDialog= new AlertDialog.Builder(ForgotPassword.this);
						altDialog.setMessage(Html.fromHtml(EMAIL_VAL));
						altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub

								dialog.cancel();
							}
						});
						altDialog.show(); 
					}else if(CommonUtils.isNetworkAvailable(getBaseContext())){


						progress.show();
						Thread dataload = new Thread(){
							@Override
							public void run() {
								//Garbage collection
								System.gc();
								forgotPasswordPressed(); // webservice call
								progress.dismiss();
								interrupt();
							}
						};
						dataload.start();
					} else {

						// Network availability label
						AlertDialog.Builder altDialog= new AlertDialog.Builder(ForgotPassword.this);
						altDialog.setMessage(""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL));
						altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub

								dialog.cancel();
							}
						});
						altDialog.show();

					}

				}else{
					//Validation message display
					AlertDialog.Builder altDialog= new AlertDialog.Builder(ForgotPassword.this);
					altDialog.setMessage(""+Html.fromHtml(FORGOT_VALLABEL));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show(); 
					//search.setError("Please fill the detail");
				}   	
			}
		});
		
		/**
		 * It is used for get a forgot password 
		 */

		email.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View view, int keyCode, KeyEvent event) {
				if (event != null && keyCode == KeyEvent.KEYCODE_ENTER) {

					if(CommonUtils.checkEmpty(email) ){
						if(!CommonUtils.isValidEmail(email.getText().toString())){
							AlertDialog.Builder altDialog= new AlertDialog.Builder(ForgotPassword.this);
							altDialog.setMessage(EMAIL_VAL);
							altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub

									dialog.cancel();
								}
							});
							altDialog.show(); 
						}else if(CommonUtils.isNetworkAvailable(getBaseContext())){


							progress.show();
							Thread dataload = new Thread(){
								@Override
								public void run() {
									//Garbage collection
									System.gc();
									forgotPasswordPressed();
									progress.dismiss();
									interrupt();
								}
							};
							dataload.start();
						} else {

							Toast.makeText(getApplicationContext(), ""+Html.fromHtml(NETWORK_UNAVAILABLE_LABEL),Toast.LENGTH_LONG).show();

						}

					}else{
						AlertDialog.Builder altDialog= new AlertDialog.Builder(ForgotPassword.this);
						altDialog.setMessage(""+Html.fromHtml(FORGOT_VALLABEL));
						altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub

								dialog.cancel();
							}
						});
						altDialog.show(); 
						//search.setError("Please fill the detail");
					}   	
					return true;
				} else {
					return false;
				}
			}
		});

		//Back button action
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = getIntent();;
				i.setClass(getApplicationContext(),Login.class);
				i.putExtra("from", "forgotpass");
				startActivity(i);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_forgot_password, menu);
		return true;
	}




	public void forgotPasswordPressed(){

		String forgotpass = getResources().getString(R.string.baseURL)+"forgotpassword?email="+URLEncoder.encode(email.getText().toString().trim());
		JSONArray jsonObject1 = CommonUtils.readJsonArray(forgotpass);
		try {
			if(jsonObject1.get(0)!=null){
				if(jsonObject1.getJSONObject(0).getString("status").equals("true")){
					msg="success";
					sendMessage(""+Html.fromHtml(FORGOT_SUCCESS));
					Intent i = getIntent();
					i.setClass(getApplicationContext(), Login.class);
					startActivity(i);
				}else{
					msg="failure";
					sendMessage(""+Html.fromHtml(EMAIL_NOTEXISTS));
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace() ;
		}  	   		 
	} 

	//Pass information to UI thread
	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			//super.handleMessage(msg);
			try{
				Bundle b = msg.getData();

				if(msg.equals("success")){
					Toast.makeText(getApplicationContext(),b.getString("message"),Toast.LENGTH_SHORT).show();
				}else{
					AlertDialog.Builder altDialog= new AlertDialog.Builder(ForgotPassword.this);
					altDialog.setMessage(b.getString("message"));
					altDialog.setNeutralButton(OK_LABEL, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.cancel();
						}
					});
					altDialog.show();
				}	

			}catch (Exception e) {
				// TODO: handle exception
			}	
		}

	};

	//send message is used pass information to handler
	public void sendMessage(String message){
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putString("message", message);
		msg.setData(b);
		handler.sendMessage(msg);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Intent intent = getIntent();
		intent.setClass(getApplicationContext(),Settings.class);
		startActivity(intent);

		return true;
	}

}

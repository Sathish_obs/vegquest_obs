package com.obs.vegquest.dbhandler;

import java.util.ArrayList;
import java.util.List; 
import com.obs.vegquest.domain.Restaurant;
import android.content.ContentValues;
import android.content.Context; 
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DbHandler extends SQLiteOpenHelper{

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "vegquest.db";

	// Product table name
	private static final String TABLE_RESTAURANT = "restaurant";

	// tags table name
	private static final String TABLE_TAGS = "tags";


	// serverupdatelog table name
	private static final String TABLE_SERVERUPDATELOG = "serverupdatelog";


	//private static final long countPerPage = new CommonConstants().countPerPage;

	//common columns
	private static final String KEY_ID       = "id";
	private static final String KEY_LASTUPDATEDATE   = "lastupdateddate";


	//city,state
	private static final String KEY_CRITERIA  = "Criteria";

	public DbHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating All Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		try{


			/*String CREATE_RESTAURANT_TABLE = "CREATE TABLE " + TABLE_RESTAURANT + "("
					+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_CITY + " TEXT,"+ KEY_STATE + " TEXT,"+KEY_LASTUPDATEDATE+" DATE)";*/

			String CREATE_RESTAURANT_TABLE = "CREATE TABLE " + TABLE_RESTAURANT + "("
					+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_CRITERIA + " TEXT,"+KEY_LASTUPDATEDATE+" DATE)";

			db.execSQL(CREATE_RESTAURANT_TABLE);


		} catch (Exception e) {
			Log.e("ERROR_ONCREATE", "ERROR_ONCREATE :"+e);
		}
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i("Inventory", "updating latest version");
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESTAURANT);



		// Create tables again
		onCreate(db);
	}



	/********* PRODUCT ********************/
	// Adding new city,state
	public void addRestaurant(String id,String criteria,String lastupdateddate) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, id);   //id
			//values.put(KEY_CITY, city); // Available
			//values.put(KEY_STATE, state); // UpdatedOn
			values.put(KEY_CRITERIA, criteria);
			values.put(KEY_LASTUPDATEDATE, lastupdateddate); // UpdatedOn
			// Inserting Row
			db.insert(TABLE_RESTAURANT, null, values);
		} catch (Exception e) {
			Log.e("ERROR_ADDRESTAURANT", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
	}

	//get last update
	public String findLastUpdatedOn(){
		List updateList = new ArrayList();
		String date = null;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			// Select All Query

			String selectQuery = "SELECT  MAX("+KEY_LASTUPDATEDATE+") FROM " + TABLE_RESTAURANT;

			Cursor cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if(cursor!=null){
				cursor.moveToFirst();
				if(cursor.getPosition()!=-1){
					date =cursor.getString(0);
				}	
			}	
			cursor.close();
			db.close();
			return date;
		}
		catch (SQLException e) {
			Log.e("ERROR_CHECKLASTUPDATE", ""+e);
			return null;
		}
	}


	// Getting Restaurant
	public Restaurant getRestaurant(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Restaurant categories = null;
		try{
			Cursor cursor = db.query(TABLE_RESTAURANT, new String[] { KEY_ID,
					KEY_CRITERIA,KEY_LASTUPDATEDATE}, KEY_ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null) {
				if(cursor.moveToFirst()) {

					categories = new Restaurant(cursor.getInt(cursor.getColumnIndex(KEY_ID)),
							cursor.getString(cursor.getColumnIndex(KEY_CRITERIA)),"",cursor.getString(cursor.getColumnIndex(KEY_LASTUPDATEDATE)));
				}
			}		 
			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_GetRestaurant", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return Category
		return categories;
	}



	// Getting Restaurant
	public List<String> getAllCityState() {
		SQLiteDatabase db = this.getReadableDatabase();
		List<String> restaurantList = new ArrayList<String>();
		try{
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_RESTAURANT;

			Cursor cursor = db.rawQuery(selectQuery, null);
			cursor.moveToFirst();
			do {

				// Adding user to list

				restaurantList.add(cursor.getString(cursor.getColumnIndex(KEY_CRITERIA)));

			} while (cursor.moveToNext());

			cursor.close();
		} catch (Exception e) {
			Log.e("ERROR_getAllCityState", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		// return Category
		return restaurantList;
	}

	// Updating single Category
	public int updateRestaurant(Restaurant restaurant) {
		int returnValue = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try{
			ContentValues values = new ContentValues();
			values.put(KEY_ID, restaurant.getId()); // category id
			values.put(KEY_CRITERIA, restaurant.getCity()); // category name
			//values.put(KEY_STATE, restaurant.getState()); // UpdatedOn
			values.put(KEY_LASTUPDATEDATE, restaurant.getLastUpdatedDate());
			// updating row
			returnValue = db.update(TABLE_RESTAURANT, values, KEY_ID + " = ?",
					new String[] { String.valueOf(restaurant.getId()) });
		} catch (Exception e) {
			Log.e("ERROR_updateRestaurant", ""+e);
		}
		finally{
			db.close(); // Closing database connection
		}
		return returnValue;
	}


}
